
/***
 * Library - JS Custom Touch Events
 *
 * Detects custom touch events and emits as DOM CustomEvents
 *
 * Event Types:
 * [pan]: mouse/touch begins on target element, and moves to anywhere in the document.
 *   - [panstart, panend] triggered at beginning and end of the move
 *   - [panleft, panright, panup, pandown] triggered at any point during the move, if mouse/touch is displaced more than the threshold
 *
 * [press]: mouse/touch begins on target element, held for a period of time.
 *   - [pressdown] triggered if held down for sufficient time, a pressdown event is triggered.
 *   - [pressup] triggered on release
 *   - at any point, if significant movement is detected, the press gesture is cancelled, i.e. pressup will not be triggered
 * 
 * [tap]: mouse/touch begins on a target element, and released quickly.
 *   - [tap] triggered if released before threshold duration, and without movement
 *   - if held down for a signficant amount of time, the tap gesture is cancelled.
 *   - at any point, if significant movement is detected, the tap gesture is cancelled.
 * 
 * [swipe]: mouse/touch begins on target element, moves, and ends anywhere in the document.
 *   - [swipeleft, swiperight, swipeup, swipedown] triggered if total displacement within the time elapsed exceeds a certain velocity in the x-axis or y-axis.
 *
 * [pinch]: 2 touches begin on target element, move and end anywhere in the document.
 *   - [pinchstart, pinchend] triggered at beginning and end of the move
 *   - [pinchin, pinchout] triggered if distance between the 2 points decreases / increases.
 *   - Note: no mouse support
 *   - Note: only 1 pinch gesture is allowed per element
 *
 * [rotate]: TODO
 *   - [rotatestart, rotateend] triggered at beginning and end of the move
 *
 * Dependencies: support for DOM CustomEvent
 ***/
(function(global) {
	'use strict';
	
	var ALL_EVENT_TYPES = 'pan press swipe tap pinch rotate';
	var DEFAULT_OPTIONS = {
		// pan
		'panXThreshold': 10, // min px required for a pan in x-axis
		'panYThreshold': 10, // min px required for a pan in y-axis
		
		// press
		'pressStrayXMax': 20, // max px of stray allowed after touch starts, before touch ends
		'pressStrayYMax': 20,
		'pressDurationThreshold': 400, //min duration (ms) required for a pressdown
		
		// tap
		'tapStrayXMax': 10, // max px of stray allowed after touch starts, before touch ends
		'tapStrayYMax': 10,
		'tapDurationMax': 450, // max duration (ms) for a tap
		
		// swipe
		'swipeXThreshold': 16, // min px required for a swipe in x-axis
		'swipeYThreshold': 16, // min px required for a swipe in y-axis
		'swipeVelocityXThreshold': 0.05, //min velocity in x-axis required for swipeleft, swiperight (px per ms)
		'swipeVelocityYThreshold': 0.05, //min velocity in y-axis required for swipeup, swipedown (px per ms)
		
		'pinchDistanceThreshold': 1.4,

		// global
		'enableMouse': true,
		'enableTouch': true,

		'touchAction': 'none' // prevents browser from handling panning (scrolling) and pinching actions (zooming)
	};
	var EVENT_LISTENER_PARAM = supportsPassiveEventListeners() ? {'passive': true} : false;
	
	// all detectors that have been set up. Each detector is {element: <HTMLElement>, eventType: <string>, namespace: <string>, and cleanup: <function>}
	var detectors = [];
	
	// all currently ongoing gestures
	var panGestures = [];
	var pressGestures = [];
	var swipeGestures = [];
	var tapGestures = [];
	var pinchGestures = [];
	var rotateGestures = [];
	
	/**
	 * Starts emitting custom events based on the type enterd
	 * - Note: native touch behaviour will be disabled
	 * @param {HTMLElement | string} elem - if string, querySelectorAll will be applied to all elements matching the string
	 *        {string} eventType - space-separated types of events [pan, press, swipe]
	 *        {object} userOptions (optional) - will be merged into default options
	 *        {string} namespace (optional)
	 */
	function detect(elem, eventType, userOptions, namespace) {
		var i, len; // tmp variables for loops
		
		// Process param: eventType
		if (typeof eventType !== 'string') {
			throw new TypeError('detect() - eventType must be string');
		}
		if (eventType.trim() === 'all') {
			eventType = ALL_EVENT_TYPES;
		}
		if (eventType.trim() === '') {
			return;
		}
		
		// Process param: elem - find the HTMLElement(s)
		if (typeof elem === 'string') {
			var elems = document.querySelectorAll(elem);
			if (elems.length === 0) {
				console.warn('detect() - no elements found matching: ' + elem);
				return;
			}
			
			for (i=0, len=elems.length; i<len; i++) {
				detect(elems[i], eventType, userOptions, namespace);
			}
			return;
		} // param is string
		else if (elem instanceof Array) {
			for (i=0, len=elem.length; i<len; i++) {
				detect(elem[i], eventType, userOptions, namespace);
			}
			return;
		} // param is array of elems
		else if (!(elem instanceof HTMLElement)) {
			throw new TypeError('detect() - elem is not an HTMLElement.');
		}
		
		// Process param: userOptions
		var options = Object.assign({}, DEFAULT_OPTIONS); // make a clone
		if (typeof userOptions === 'object') {
			for (var key in options) {
				if (options.hasOwnProperty(key) && typeof userOptions[key] === typeof options[key]) {
					options[key] = userOptions[key];
				} // update only if compatible
			} // go through each option setting
		}
		
		// Process param: namespace
		if (typeof namespace === 'string' && namespace.length > 0) {
			if (!/^[a-zA-Z]+$/.test(namespace)) {
				throw new RangeError('detect() - namespace must be alphabetical. Received: "' + namespace +'"');
			}
			namespace = '.' + namespace; // prepend dot to namespace
		}
		else {
			namespace = '';
		}

		// Find all event types to be detected
		var eventTypeArray = eventType.split(' ');
		eventTypeArray = eventTypeArray.filter(function(item, pos) { // remove duplicates
    		return eventTypeArray.indexOf(item) === pos;
		});

		// Start detection for each event type
		var detector; // to be created for each event
		for (i=0, len=eventTypeArray.length; i<len; i++) {
			eventTypeArray[i] = eventTypeArray[i].trim();

			switch (eventTypeArray[i]) {
				case 'pan':
					if (!isDetecting(elem, 'pan', namespace)) {
						detector = createPanDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				
				case 'press':
					if (!isDetecting(elem, 'press', namespace)) {
						detector = createPressDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				case 'swipe':
					if (!isDetecting(elem, 'swipe', namespace)) {
						detector = createSwipeDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				
				case 'tap':
					if (!isDetecting(elem, 'tap', namespace)) {
						detector = createTapDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
					
				case 'pinch':
					if (!isDetecting(elem, 'pinch', namespace)) {
						detector = createPinchDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				default:
					console.warn('detect() - unrecognised eventType ' + eventTypeArray[i]);
					break;
			} //switch
		} // for each event type

		// Override native touch behaviour
		elem.style.webkitUserSelect = 'none';
		elem.style.mozUserSelect = 'none';
		elem.style.msUserSelect = 'none';
		elem.style.userSelect = 'none';
		elem.style.webkitTouchCallout = 'none';
		elem.style.webkitUserDrag = 'none';
		elem.style.userDrag = 'none';
		elem.style.webkitTapHighlightColor = 'rgba(0,0,0,0)';
		elem.style.msTouchSelect = 'none';
		elem.style.msContentZooming = 'none';
		elem.style.msTouchAction = options['touchAction'];
		elem.style.touchAction = options['touchAction'];
		if (eventTypeArray.includes('pinch') && options['touchAction'].includes('pinch-zoom')) {
			console.warn('Pinch-zoom touch-action is still enabled. Pinch detection may not work correctly.');
		}

		// Always prevent drag to ensure element doesn't move around during detection of gestures
		elem.addEventListener('dragstart', preventDefault);
	} //detect()
	
	/**
	 * Stops detecting custom events
	 * @param {HTMLElement | string} elem - if string, querySelectorAll will be applied to all elements matching the string
	 *        {string} eventType - space-separated types of events [pan, press, swipe]
	 *        {string} namespace (optional)
	 */
	function stopDetect(elem, eventType, namespace) {
		var i, len; // tmp variables for loops
		
		// Process param: eventType
		if (typeof eventType !== 'string') {
			throw new TypeError('stopDetect() - type must be string');
		}
		if (eventType.trim() === 'all') {
			eventType = ALL_EVENT_TYPES;
		}
		
		// Process param: elem - find the HTMLElement
		if (typeof elem === 'string') {
			var elems = document.querySelectorAll(elem);
			if (elems.length === 0) {
				console.warn('stopDetect() - no elements found matching: ' + elem);
				return;
			}
			
			for (i=0, len=elems.length; i<len; i++) {
				stopDetect(elems[i], eventType, namespace);
			}
			return;
		} // param is string
		else if (elem instanceof Array) {
			for (i=0, len=elem.length; i<len; i++) {
				stopDetect(elem[i], eventType, namespace);
			}
			return;
		} // param is array of elems
		else if (!(elem instanceof HTMLElement)) {
			throw new TypeError('stopDetect() - elem is not an HTMLElement.');
		}
		
		// Process param: namespace
		if (typeof namespace === 'string' && namespace.length > 0) {
			if (!/^[a-zA-Z]+$/.test(namespace)) {
				throw new RangeError('stopDetect() - namespace must be alphabetical. Received: "' + namespace +'"');
			}
			namespace = '.' + namespace; // prepend dot to namespace
		}
		else {
			namespace = '';
		}
		
		// Find all event types to be detected
		var eventTypeArray = eventType.split(' ');
		eventTypeArray = eventTypeArray.filter(function(item, pos) { // remove duplicates
    		return eventTypeArray.indexOf(item) === pos;
		});

		// Stop detector for each event type
		var detector;
		for (i=0, len=eventTypeArray.length; i<len; i++) {
			eventTypeArray[i] = eventTypeArray[i].trim();

			//detector = getDetector(elem, eventTypeArray[i], namespace);
			detector = detectors.find(function(v) {
				return v.element === elem && v.eventType === eventTypeArray[i] && v.namespace === namespace;
			});
			if (!detector) {
				continue;
			}

			//console.debug('cleaning up ' +  elem.id + ' ' + eventTypeArray[i] + ' null? ' +  (detector === null));
			detector.cleanup();
			removeDetector(detector);
		}

		elem.removeEventListener('dragstart', preventDefault);
	} //stopDetect()
	
	/**
	 * @return {number} of detectors set up
	 */
	function getDetectorCount() {
		return detectors.length;
	}
	
	/**
	 * @return {number} of pan gestures currently being tracked
	 */
	function getPanGestureCount() {
		return panGestures.length;
	}
	
	/**
	 * @return {number} of press gestures currently being tracked
	 */
	function getPressGestureCount() {
		return pressGestures.length;
	}
	
	/**
	 * @return {number} of swipe gestures currently being tracked
	 */
	function getSwipeGestureCount() {
		return swipeGestures.length;
	}
	
	/**
	 * @return {number} of tap gestures currently being tracked
	 */
	function getTapGestureCount() {
		return tapGestures.length;
	}

	/**
	 * @return {number} of pinch gestures currently being tracked
	 */
	function getPinchGestureCount() {
		return pinchGestures.length;
	}

	/**
	 * @return {number} of rotate gestures currently being tracked
	 */
	function getRotateGestureCount() {
		return rotateGestures.length;
	}


/*****************
 *   INTERNALS   *
 *****************/
 	/**
 	 * @private
 	 * Checks if the browser supports passive event listeners (e.g. Chrome 58+)
 	 * @return {boolean}
 	 */
 	function supportsPassiveEventListeners() {
		var support = false;
		try {
			var opts = Object.defineProperty({}, 'passive', {
				'get': function() {support = true;}
			});

			global.addEventListener('test', null, opts);
		} catch (e) {}
		
		return support;
 	} // supportsPassiveEventListeners()

 	/**
 	 * @private
 	 * Used for multiple listeners
 	 */ 
 	function preventDefault(e){
 		e.preventDefault();
 	}

	/**
	 * @private
	 * Starts emitting custom events: [panstart, panend] + [panleft, panright, panup, pandown]
	 * @param {HTMLElement} elem 
	 *        {object} options (optional) - {panXThreshold, panYThreshold, enableMouse, enableTouch}
	 *        {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createPanDetection(elem, options, namespace) {
		
		function onPanGestureStart(e) {
			// Ignore this start event if a pan gesture already exists for this element. (Don't let multi-touch confuse pan)
			var gestures = panGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length > 0) {
				return;
			}
			
			// Find out touch ID and position
			var touchID, touchStartPosition;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				// only handle left mouse
				if (e.which !== 1) {
					return;
				}
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse
			
			// Create gesture
			panGestures.push({
				'element': elem,
				'touchID': touchID,
				'touchStartPosition': touchStartPosition,
				'displacedX': null,
				'displacedY': null,
				'panStarted': false
			});
		} // onPanGestureStart()
		
		function onPanGestureMove(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this move event if no pan gestures exist.
			var gestures = panGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			var gesture, currPosition;
			if (e.type === 'touchmove') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(panGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				} // for all changed touches
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(panGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}
			
			// Calculate distance moved
			var displacedX = currPosition[0] - gesture['touchStartPosition'][0];
			var displacedY = currPosition[1] - gesture['touchStartPosition'][1];
			if (gesture['displacedX'] === displacedX && gesture['displacedY'] === displacedY) {
				return;
			} // no movement
			
			// Update displacement
			gesture['displacedX'] = displacedX;
			gesture['displacedY'] = displacedY;
			var eventInitDict = {
				'detail': {
					'displacedX': displacedX,
					'displacedY': displacedY
				}
			};
			
			// Emit pan event if threshold exceeded
			var thresholdExceeded = false;
			if (displacedX > options['panXThreshold']) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panright'+namespace, eventInitDict));
			} // pan right detected
			else if (0 - displacedX > options['panXThreshold']) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panleft'+namespace, eventInitDict));
			} //pan left detected
			if (displacedY > options['panYThreshold']) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('pandown'+namespace, eventInitDict));
			} // pan down detected
			else if (0-displacedY > options['panYThreshold']) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panup'+namespace, eventInitDict));
			} //pan up detected
			//console.debug('thresholdHold exceeded?: ' + thresholdExceeded);
			
			// Send panstart event if this is the first one
			if (thresholdExceeded && !gesture['panStarted']) {
				gesture['panStarted'] = true;
				
				eventInitDict.detail.startX = gesture['touchStartPosition'][0];
				eventInitDict.detail.startY = gesture['touchStartPosition'][1];
				elem.dispatchEvent(new CustomEvent('panstart'+namespace, eventInitDict));
			}
		} //onPanGestureMove()
		
		function onPanGestureEnd(e) {
			var i, len; // tmp variables for loops

			// Ignore this end event if no pan gestures exist.
			var gestures = panGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture, currPosition;
			if (e.type === 'touchend') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(panGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(panGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}
			
			// Emit panend (but only if panstart has previously been emitted)
			if (gesture['panStarted']) {
				elem.dispatchEvent(new CustomEvent('panend'+namespace, {
					'detail': {
						'displacedX': gesture['displacedX'],
						'displacedY': gesture['displacedY'],
						'endX': currPosition[0],
						'endY': currPosition[1]
					}
				}));
			}
			removeGesture(panGestures, gesture);
		} //onPanGestureEnd()
		
		function onPanGestureCancel(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this cancel event if no pan gestures exist.
			var gestures = panGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event
			var gesture;
			if (e.type === 'touchcancel') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(panGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(panGestures, elem, null);
			} // mouse			
			if (!gesture) {
				return;
			}

			removeGesture(panGestures, gesture);
		} //onPanGestureCancel()		
		
		function setUpPanListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onPanGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onPanGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onPanGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onPanGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.addEventListener('mousedown', onPanGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mousemove', onPanGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onPanGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // setUpPanListeners()
		function tearDownPanListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onPanGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onPanGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onPanGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onPanGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onPanGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mousemove', onPanGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onPanGestureEnd, EVENT_LISTENER_PARAM);
			}
		} //tearDownPanListeners()			
		
		setUpPanListeners();
		return {
			'eventType': 'pan',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownPanListeners
		};
	} //createPanDetection()
	
	/**
	 * @private
	 * Starts emitting custom events: [pressdown, pressup]
	 * @param {HTMLElement} elem 
	 *        {object} options (optional) - {pressDurationThreshold, pressStrayXMax, pressStrayYMax, enableMouse, enableTouch}
	 *        {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createPressDetection(elem, options, namespace) {
		
		function onPressGestureStart(e) {
			var touchID, touchStartPosition, touchStartTime, timerID;

			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				if (e.which !== 1) {
					return;
				} // left mouse
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse
			touchStartTime = Date.now();
			timerID = setTimeout(function(touchID) {
				//console.debug('timeout at ' + Date.now());
				
				elem.dispatchEvent(new CustomEvent('pressdown'+namespace, {
					'detail': {
						'clientX': touchStartPosition[0],
						'clientY': touchStartPosition[1]
					}
				}));
			}, options['pressDurationThreshold'], touchID);
			//console.debug(e.type+' touchID: ' + touchID + ', startTime: ' + touchStartTime + ', timerID: ' + timerID);
			
			// Create gesture
			pressGestures.push({
				'element': elem,
				'touchID': touchID,
				'touchStartPosition': touchStartPosition,
				'touchStartTime': touchStartTime,
				'timerID': timerID
			});
		} //onPressGestureStart()
		
		function onPressGestureMove(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this move event if no pan gestures exist.
			var gestures = pressGestures.filter(function(g){
				return g.element === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchmove') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(pressGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(pressGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}
			
			// Calculate movement
			var displacedX = currPosition[0] - gesture['touchStartPosition'][0];
			var displacedY = currPosition[1] - gesture['touchStartPosition'][1];
			
			if (displacedX > options['pressStrayXMax'] || 0-displacedX > options['pressStrayXMax'] ||
			    displacedY > options['pressStrayYMax'] || 0-displacedY > options['pressStrayYMax']) {
				//console.debug('press detector cancelled due to move, touchID: ' + gesture['touchID']);
				clearTimeout(gesture['timerID']);
				removeGesture(pressGestures, gesture);
			}
		} //onPressGestureMove()
		
		function onPressGestureEnd(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this end event if no press gestures exist.
			var gestures = pressGestures.filter(function(g){
				return g.element === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchend') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(pressGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(pressGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}
			
			// Emit "pressup" if duration exceeds threshold
			var duration = Date.now() - gesture['touchStartTime'];
			if (duration > options['pressDurationThreshold']) {
				elem.dispatchEvent( new CustomEvent('pressup'+namespace, {
					'detail': {
						'clientX': currPosition[0],
						'clientY': currPosition[1],
						'duration': duration
					}
				}));
			}
			
			clearTimeout(gesture['timerID']);
			removeGesture(pressGestures, gesture);
		} //onPressGestureEnd()
		
		function onPressGestureCancel(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this cancel event if no press gestures exist.
			var gestures = pressGestures.filter(function(g){
				return g.element === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event
			var gesture;
			if (e.type === 'touchcancel') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(pressGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(pressGestures, elem, null);
			} // mouse
			if (!gesture) {
				return;
			}

			removeGesture(pressGestures, gesture);
			//console.debug('press detector cancelled due to ' + e.type + ', touchID: ' + gesture['touchID']);
		} //onPressGestureCancel()
		
		function setUpPressListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onPressGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onPressGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onPressGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onPressGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.addEventListener('mousedown', onPressGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mousemove', onPressGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onPressGestureEnd, EVENT_LISTENER_PARAM);
			}
		} //setUpPressListeners()
		function tearDownPressListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onPressGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onPressGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onPressGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onPressGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onPressGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mousemove', onPressGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onPressGestureEnd, EVENT_LISTENER_PARAM);
			}
		} //tearDownPressListeners()
		
		setUpPressListeners();
		return {
			'eventType': 'press',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownPressListeners
		};
	} //createPressDetection()
	
	/**
	 * @private
	 * Starts emitting custom events: [swipeleft, swiperight, swipeup, swipedown]
	 * @param {HTMLElement} elem 
	 *        {object} options (optional) - {swipeXThreshold, swipeYThreshold, swipeVelocityXThreshold, swipeVelocityYThreshold, enableMouse, enableTouch}
	 *        {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createSwipeDetection(elem, options, namespace) {
		
		function onSwipeGestureStart(e) {
			// Find out touch ID, position, startTime
			var touchID, touchStartPosition, touchStartTime;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				if (e.which !== 1) {
					return;
				} // left mouse
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse
			touchStartTime = Date.now();
			//console.debug(e.type+' touchID: ' + touchID + ', startTime: ' + touchStartTime);
			
			// Create gesture
			swipeGestures.push({
				'element': elem,
				'touchID': touchID,
				'touchStartPosition': touchStartPosition,
				'touchStartTime': touchStartTime
			});
		} //onSwipeGestureStart()
		
		function onSwipeGestureEnd(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this end event if no swipe gestures exist.
			var gestures = swipeGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture, currPosition;
			if (e.type === 'touchend') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(swipeGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(swipeGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}
			
			// Calculate duration, displacement, velocity
			var duration = Date.now() - gesture['touchStartTime'];
			var displacedX = currPosition[0] - gesture['touchStartPosition'][0];
			var displacedY = currPosition[1] - gesture['touchStartPosition'][1];
			var velocityX = displacedX / duration;
			var velocityY = displacedY / duration;
			
			// Send swipe events (if threshold exceeeded)
			var eventInitDict = {
				'detail': {
					'startX': gesture['touchStartPosition'][0],
					'startY': gesture['touchStartPosition'][1],
					'endX': currPosition[0],
					'endY': currPosition[1],
					'velocityX': velocityX,
					'velocityY': velocityY,
					'duration': duration
				}
			};
			if (velocityX > options['swipeVelocityXThreshold'] && displacedX > options['swipeXThreshold']) {
				elem.dispatchEvent(new CustomEvent('swiperight'+namespace, eventInitDict));
			}
			
			if (0-velocityX > options['swipeVelocityXThreshold'] && 0-displacedX > options['swipeXThreshold']) {
				elem.dispatchEvent(new CustomEvent('swipeleft'+namespace, eventInitDict));
			}
			
			if (velocityY > options['swipeVelocityYThreshold'] && displacedY > options['swipeYThreshold']) {
				elem.dispatchEvent(new CustomEvent('swipedown'+namespace, eventInitDict));
			}
			
			if (0-velocityY > options['swipeVelocityYThreshold'] && 0-displacedY > options['swipeYThreshold']) {
				elem.dispatchEvent(new CustomEvent('swipeup'+namespace, eventInitDict));
			}
			
			removeGesture(swipeGestures, gesture);
		} //onSwipeGestureEnd()
		
		function onSwipeGestureCancel(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this cancel event if no swipe gestures exist.
			var gestures = swipeGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event
			var gesture;
			if (e.type === 'touchcancel') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(swipeGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(swipeGestures, elem, null);
			} // mouse
			if (!gesture) {
				return;
			}

			removeGesture(swipeGestures, gesture);
		} //onSwipeGestureCancel()
		
		function setUpSwipeListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onSwipeGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onSwipeGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onSwipeGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.addEventListener('mousedown', onSwipeGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onSwipeGestureEnd, EVENT_LISTENER_PARAM);
			}
		} //setUpSwipeListeners()
		function tearDownSwipeListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onSwipeGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onSwipeGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onSwipeGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onSwipeGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onSwipeGestureEnd, EVENT_LISTENER_PARAM);
			}
		} //tearDownSwipeListeners()
		
		setUpSwipeListeners();
		
		return {
			'eventType': 'swipe',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownSwipeListeners
		};
	} // createSwipeDetection()
	
	/**
	 * @private
	 * Starts emitting custom events: [tap]
	 * @param {HTMLElement} elem 
	 *         {object} options (optional) - {tapDurationThreshold, tapStrayXMax, tapStrayYMax, enableMouse, enableTouch}
	 *         {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createTapDetection(elem, options, namespace) {
		// Handlers (Touch Events)
		function onTapGestureStart(e) {
			// Find out touch ID, position, start time
			var touchID, touchStartPosition, touchStartTime;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				if (e.which !== 1) {
					return;
				} // left mouse
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse
			touchStartTime = Date.now();
			//console.debug(e.type+' touchID: ' + touchID);
			
			// Create gesture
			tapGestures.push({
				'element': elem,
				'touchID': touchID,
				'touchStartPosition': touchStartPosition,
				'touchStartTime': touchStartTime
			});
		} //onTapGestureStart()
		
		function onTapGestureMove(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this move event if no tap gestures exist.
			var gestures = tapGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture, currPosition;
			if (e.type === 'touchmove') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(tapGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(tapGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}
			
			// Calculate movement
			var displacedX = currPosition[0] - gesture['touchStartPosition'][0];
			var displacedY = currPosition[1] - gesture['touchStartPosition'][1];
			
			if (displacedX > options['tapStrayXMax'] || 0-displacedX > options['tapStrayXMax'] ||
			    displacedY > options['tapStrayYMax'] || 0-displacedY > options['tapStrayYMax']) {
				//console.debug('tap detector cancelled due to move, touchID: ' + gesture['touchID']);
				removeGesture(tapGestures, gesture);
			}
		} //onTapGestureMove()
		
		function onTapGestureEnd(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this end event if no tap gestures exist.
			var gestures = tapGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture, currPosition;
			if (e.type === 'touchend') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(tapGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(tapGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}
			
			// Emit "tap" if touch duration is within threshold
			var duration = Date.now() - gesture['touchStartTime'];
			if (duration < options['tapDurationMax']) {
				elem.dispatchEvent(new CustomEvent('tap'+namespace, {
					'detail': {
						'startX': gesture['touchStartPosition'][0],
						'startY': gesture['touchStartPosition'][1],
						'endX': currPosition[0],
						'endY': currPosition[1]
					}
				}));
			}
			
			removeGesture(tapGestures, gesture);
		} //onTapGestureEnd()
		
		function onTapGestureCancel(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this cancel event if no tap gestures exist.
			var gestures = tapGestures.filter(function(gesture){
				return gesture['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			if (e.type === 'touchcancel') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					gesture = findGestureByElementAndTouchID(tapGestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(tapGestures, elem, null);
			} // mouse
			if (!gesture) {
				return;
			}

			removeGesture(tapGestures, gesture);
		} //onTapGestureCancel()
		
		function setUpTapListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onTapGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onTapGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onTapGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onTapGestureCancel, EVENT_LISTENER_PARAM);
			}

			// Some browsers emulates mouse events on touchstart/touchend
			// If they support touch events, we should not enable mouse
			if (options['enableMouse'] && !('ontouchstart' in document.documentElement)) {
				elem.addEventListener('mousedown', onTapGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mousemove', onTapGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onTapGestureEnd, EVENT_LISTENER_PARAM);
			}
		} //setUpTapListeners()
		function tearDownTapListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onTapGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onTapGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onTapGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onTapGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onTapGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mousemove', onTapGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onTapGestureEnd, EVENT_LISTENER_PARAM);
			}
		} //tearDownTapListeners()
		
		setUpTapListeners();
		
		return {
			'eventType': 'tap',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownTapListeners
		};
	} //createTapDetection()

	/**
	 * @private
	 * Starts emitting custom events: [pinchstart, pinchend] + [pinchin, pinchout]
	 * @param {HTMLElement} elem 
	 *        {Object=} options - {}
	 *        {string} namespace - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createPinchDetection(elem, options, namespace) {

		function onTouchStart(e) {
			var touch0ID, touch1ID, touch0Position, touch1Position, touchStartTime, distance;

			// Check if we have at least 2 touch points
			if (e.touches.length < 2) {
				//console.debug('Less than 2 touch points - no pinch gesture started');
				return;
			}
			//console.debug('More than 2 touch points -  pinch gesture ready to start');

			// Check if we already have a pinch gesture
			var gesture = pinchGestures.find(function(gesture){
				return gesture['element'] === elem;
			});
			if (gesture) {
				//console.debug('Already have pinch gesture - not creating more');
				return;
			}


			touch0ID = e.touches[0].identifier;
			touch1ID = e.touches[1].identifier;
			touch0Position = [e.touches[0].clientX, e.touches[0].clientY];
			touch1Position = [e.touches[1].clientX, e.touches[1].clientY];
			touchStartTime = Date.now();
			distance = getDistance(touch0Position, touch1Position);

			// Create gesture
			pinchGestures.push({
				'element': elem,
				'touch0ID': touch0ID,
				'touch1ID': touch1ID,
				'touch0StartX': touch0Position[0],
				'touch0StartY': touch0Position[1],
				'touch1StartX': touch1Position[0],
				'touch1StartY': touch1Position[1],
				'touchStartTime': touchStartTime,
				'startDistance': distance,
				'prevDistance': distance,
				'pinchStarted': false
			});
			//console.debug('Created pinch gesture for ID:' + touch0ID + ' (' + touch0Position[0] + ', ' +  touch0Position[1] + ') and ID:' + touch1ID + ' (' + touch1Position[0] + ', ' +  touch1Position[1] + '). Distance: ' + distance);
		} // onTouchStart()

		function onTouchMove(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this move event if no pinch gestures exist.
			var gesture = pinchGestures.find(function(gesture){
				return gesture['element'] === elem;
			});
			if (!gesture) {
				return;
			}

			// Find the touches for this gesture
			var touch0, touch1;
			for (i=0, len=e.touches.length; i<len; i++) {
				if (e.touches[i].identifier === gesture['touch0ID']) {
					touch0 = e.touches[i];
				}
				if (e.touches[i].identifier === gesture['touch1ID']) {
					touch1 = e.touches[i];
				}
			} // for all changed touches
			if (!touch0 || !touch1) {
				//console.debug('Could not find both touches: ' + !!touch0 + ', ' + !!touch1);
				return;
			}

			// Check if pinch has occurred (distance changed)
			var curr_distance = getDistance([touch0.clientX, touch0.clientY], [touch1.clientX, touch1.clientY]);
			var delta_distance = Math.abs(curr_distance - gesture['prevDistance']);
			//console.debug('Moved ' + delta_distance);
			if (delta_distance < options['pinchDistanceThreshold']) {
				return;
			}

			// Send pinchStart if not yet sent
			if (!gesture['pinchStarted']) {
				gesture['pinchStarted'] = true;

				elem.dispatchEvent(new CustomEvent('pinchstart'+namespace, eventInitDict));
			}

			// Emit "pinchin" / "pinchout" accordingly
			var eventInitDict = {
				'detail': {
					'distance': curr_distance,
					'startDistance': gesture['startDistance'],
					'zoom': curr_distance / gesture['startDistance']
				}
			};
			if (curr_distance > gesture['prevDistance']) {
				elem.dispatchEvent(new CustomEvent('pinchout'+namespace, eventInitDict));
			} // increase
			else if (curr_distance < gesture['prevDistance']) {
				elem.dispatchEvent(new CustomEvent('pinchin'+namespace, eventInitDict));
			} // decrease
			gesture['prevDistance'] = curr_distance;			
		} // onTouchMove()

		function onTouchEnd(e) {
			// Ignore this cancel event if no tap gestures exist.
			var gesture = pinchGestures.find(function(gesture){
				return gesture['element'] === elem;
			});
			if (!gesture) {
				return;
			}

			// Check if we have at least 2 touch points
			if (e.touches.length < 2) {
				//console.debug('Less than 2 touch points. ' + e.touches.length + ' Removing pinch gesture for this element.');
				// Emit pinchend (but only if pinchstart has previously been emitted)
				if (gesture['pinchStarted']) {
					elem.dispatchEvent(new CustomEvent('pinchend'+namespace, {
						'detail': {
							'endDistance': gesture['prevDistance'],
							'zoom': gesture['prevDistance'] / gesture['startDistance']
						}
					}));
				}

				removeGesture(pinchGestures, gesture);

				return;
			}
		} // onTouchEnd()

		function onTouchCancel(e) {
			// Ignore this cancel event if no tap gestures exist.
			var gesture = pinchGestures.find(function(gesture){
				return gesture['element'] === elem;
			});
			if (!gesture) {
				return;
			}

			// Check if we have at least 2 touch points
			if (e.touches.length < 2) {
				//console.debug('Less than 2 touch points. ' + e.touches.length + ' Removing pinch gesture for this element.');
				// Emit pinchend (but only if pinchstart has previously been emitted)
				if (gesture['pinchStarted']) {
					elem.dispatchEvent(new CustomEvent('pinchend'+namespace, {
						'detail': {
							'endDistance': gesture['prevDistance'],
							'zoom': gesture['prevDistance'] / gesture['startDistance']
						}
					}));
				}

				removeGesture(pinchGestures, gesture);

				return;
			}
		} // onTouchCancel()

		function setUpTapListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onTouchStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onTouchMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onTouchEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onTouchCancel, EVENT_LISTENER_PARAM);
			}
		} //setUpTapListeners()
		function tearDownTapListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onTouchStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onTouchMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onTouchEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onTouchCancel, EVENT_LISTENER_PARAM);
			}
		} //tearDownTapListeners()

		setUpTapListeners();
		
		return {
			'eventType': 'pinch',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownTapListeners
		};
	} // createPinchDetection()
	

/*****************
 *    HELPERS    *
 *****************/
	/**
	 * @private
	 * Gets distance between 2 points
	 * @param {Array} p1
	 *        {Array} p2
	 * @return {number}
	 */
	function getDistance(p1, p2) {
		var x = p1[0] - p2[0];
		var y = p1[1] - p2[1];
		return Math.sqrt(x*x + y*y);
	} //getDistance()
	
	/**
	 * @private
	 * Checks if a detector for an element is already set up
	 * @param {HTMLElement} elem
	 *        {string} eventType
	 *        {string} namespace
	 * @return {boolean}
	 */
	function isDetecting(elem, eventType, namespace) {
		var i, len; // tmp variables for loops
		for (i=0, len=detectors.length; i<len; i++) {
			var detector = detectors[i];
			if (detector['element'] === elem && detector['eventType'] === eventType && detector['namespace'] === namespace) {
				return true;
			}
		}
		return false;
	} //isDetecting()
	
	/**
	 * @private
	 * Removes a detector
	 */
	function removeDetector(detectorToRemove) {
		var i, len; // tmp variables for loops
		for (i=0, len=detectors.length; i<len; i++) {
			var detector = detectors[i];
			if (detector['element'] === detectorToRemove['element'] && detector['eventType'] === detectorToRemove['eventType'] && detector['namespace'] === detectorToRemove['namespace']) {
				detectors.splice(i, 1);
				return;
			}
		}
	} //removeDetector()
	
	/**
	 * @private
	 * Finds a Gesture by its element and touchID
	 * @param {Array} gestures - the list of gestures to search from, e.g. panGestures
	 *        {HTMLElement} elem
	 *        {number} touchID
	 * @return {Object | null} Gesture object 
	 */
	function findGestureByElementAndTouchID(gestures, elem, touchID) {
		var i, len; // tmp variables for loops
		for (i=0, len=gestures.length; i<len; i++) {
			var gesture = gestures[i];
			if (gesture['element'] === elem && gesture['touchID'] === touchID) {
				return gesture;
			}
		}
		return null;
	} //findGestureByElementAndTouchID()
	
	/**
	 * @private
	 * Removes a Gesture from its registry
	 * @param {Array} gestures - the list of gestures to search from, e.g. panGestures
	 *        {Object} gestureToRemove
	 */
	function removeGesture(gestures, gestureToRemove) {
		var i, len; // tmp variables for loops

		for (i=0, len=gestures.length; i<len; i++) {
			var gesture = gestures[i];
			if (gesture['element'] === gestureToRemove['element'] && gesture['touchID'] === gestureToRemove['touchID']) {
				gestures.splice(i, 1);
				return;
			}
		}
	} //removeGesture()
	
	global['tove'] = {
		'detect': detect,
		'stopDetect': stopDetect,
		'getDetectorCount': getDetectorCount,
		'getPanGestureCount': getPanGestureCount,
		'getPressGestureCount': getPressGestureCount,
		'getSwipeGestureCount': getSwipeGestureCount,
		'getTapGestureCount': getTapGestureCount,
		'getPinchGestureCount': getPinchGestureCount,
		'getRotateGestureCount': getRotateGestureCount
	};
})(this);
