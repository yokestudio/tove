/***
 * Library - JS Custom Touch Events
 *
 * Detects custom touch events and emits as DOM CustomEvents
 *
 * Event Types:
 * [Pan]: pointer/touch begins on target element, and moves to anywhere in the document.
 *   - [panstart, panend] triggered at beginning and end of the move
 *   - [panleft, panright, panup, pandown] triggered at any point during the move, if pointer/touch is displaced more than the threshold
 *
 * [Press]: pointer/touch begins on target element, held for a period of time.
 *   - [pressdown] triggered if held down for sufficient time, a pressdown event is triggered.
 *   - [pressup] triggered on release
 *   - at any point, if significant movement is detected, the press gesture is cancelled, i.e. pressup will not be triggered
 * 
 * [Tap]: pointer/touch begins on a target element, and released quickly.
 *   - [tap] triggered if released before threshold duration, and without movement
 *   - if held down for a signficant amount of time, the tap gesture is cancelled.
 *   - at any point, if significant movement is detected, the tap gesture is cancelled.
 * 
 * [Swipe]: Pointer/touch begins on target element, moves, and ends anywhere in the document.
 *   - [swipeleft, swiperight, swipeup, swipedown] triggered if total displacement within the time elapsed exceeds a certain velocity in the x-axis or y-axis.
 *
 * Dependencies: support for DOM CustomEvent
 ***/
(function(global) {
	'use strict';
	
	var ALL_EVENT_TYPES = 'pan press swipe tap';
	var DEFAULT_OPTIONS = {
		// pan
		panXThreshold: 10, // min px required for a pan in x-axis
		panYThreshold: 10, // min px required for a pan in y-axis
		
		// press
		pressStrayXMax: 20, // max px of stray allowed after touch starts, before touch ends
		pressStrayYMax: 20,
		pressDurationThreshold: 500, //min duration (ms) required for a pressdown
		
		// tap
		tapStrayXMax: 10, // max px of stray allowed after touch starts, before touch ends
		tapStrayYMax: 10,
		tapDurationMax: 450, // max duration (ms) for a tap
		
		// swipe
		swipeXThreshold: 20, // min px required for a swipe in x-axis
		swipeYThreshold: 20, // min px required for a swipe in y-axis
		swipeVelocityXThreshold: 0.7, //min velocity in x-axis required for swipeleft, swiperight (px per ms)
		swipeVelocityYThreshold: 0.7, //min velocity in y-axis required for swipeup, swipedown (px per ms)
		
		// global
		enableMouse: true,
		enableTouch: true,
		enablePen: true
	};
	
	// all detectors set up {element: <HTMLElement>, eventType: <string>, namespace: <string>, and cleanup: <function>}
	var detectors = [];
	
	// all currently ongoing gestures
	var panGestures = [];
	var pressGestures = [];
	var swipeGestures = [];
	var tapGestures = [];
	
	/**
	 * Starts emitting custom events based on the type enterd
	 * - Note: native touch behaviour will be disabled
	 * @param: {HTMLElemet | string} elem - if string, querySelectorAll will be applied to all elements matching the string
	 *         {string} eventType - space-separated types of events [pan, press, swipe]
	 *         {object} userOptions (optional) - will be merged into default options
	 *         {string} namespace (optional)
	 */
	function detect(elem, eventType, userOptions, namespace) {
		var i, len; // tmp variables for loops
		
		// Process param: eventType
		if (typeof eventType !== 'string') {
			throw new TypeError('detect() - eventType must be string');
		}
		if (eventType.trim() === 'all') {
			eventType = ALL_EVENT_TYPES;
		}
		if (eventType.trim() === '') {
			return;
		}
		
		// Process param: elem - find the HTMLElement(s)
		if (typeof elem === 'string') {
			var elems = document.querySelectorAll(elem);
			if (elems.length === 0) {
				console.warn('detect() - no elements found matching: ' + elem);
				return;
			}
			
			for (i=0, len=elems.length; i<len; i++) {
				detect(elems[i], eventType, userOptions, namespace);
			}
			return;
		} // param is string
		else if (elem instanceof Array) {
			for (i=0, len=elem.length; i<len; i++) {
				detect(elem[i], eventType, userOptions, namespace);
			}
			return;
		} // param is array of elems
		else if (!(elem instanceof HTMLElement)) {
			throw new TypeError('detect() - elem is not an HTMLElement.');
		}
		
		// Process param: userOptions
		var options = cloneDefaultOptions();
		if (typeof userOptions === 'object') {
			for (var key in options) {
				if (options.hasOwnProperty(key) && typeof userOptions[key] === typeof options[key]) {
					options[key] = userOptions[key];
				} // update only if compatible
			} // go through each option setting
		}
		
		// Process param: namespace
		if (typeof namespace === 'string' && namespace.length > 0) {
			if (!/^[a-zA-Z]+$/.test(namespace)) {
				throw new RangeError('detect() - namespace must be alphabetical. Received: "' + namespace +'"');
			}
			namespace = '.' + namespace; // prepend dot to namespace
		}
		else {
			namespace = '';
		}
		
		disableNativeTouchBehaviour(elem);
		
		// Start detector for each event type
		var detector; // to be created for each event
		var eventTypeArray = eventType.split(' ');
		for (i=0, len=eventTypeArray.length; i<len; i++) {
			switch (eventTypeArray[i]) {
				case 'pan':
					if (!isDetecting(elem, 'pan', namespace)) {
						detector = createPanDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				
				case 'press':
					if (!isDetecting(elem, 'press', namespace)) {
						detector = createPressDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				case 'swipe':
					if (!isDetecting(elem, 'swipe', namespace)) {
						detector = createSwipeDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				
				case 'tap':
					if (!isDetecting(elem, 'tap', namespace)) {
						detector = createTapDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
					
				/* TODO:
				case 'pinch':
					if (!isDetecting(elem, 'pinch', namespace)) {
						var detector = detectPinch(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				*/
				default:
					console.warn('detect() - unrecognised eventType ' + eventTypeArray[i]);
					break;
			} //switch
		}
	} //detect()
	
	/**
	 * Stops detecting custom events
	 * @param: {HTMLElemet | string} elem - if string, querySelectorAll will be applied to all elements matching the string
	 *         {string} eventType - space-separated types of events [pan, press, swipe]
	 *         {string} namespace (optional)
	 */
	function stopDetect(elem, eventType, namespace) {
		var i, len; // tmp variables for loops
		
		// Process param: eventType
		if (typeof eventType !== 'string') {
			throw new TypeError('stopDetect() - type must be string');
		}
		if (eventType.trim() === 'all') {
			eventType = ALL_EVENT_TYPES;
		}
		
		// Process param: elem - find the HTMLElement
		if (typeof elem === 'string') {
			var elems = document.querySelectorAll(elem);
			if (elems.length === 0) {
				console.warn('stopDetect() - no elements found matching: ' + elem);
				return;
			}
			
			for (i=0, len=elems.length; i<len; i++) {
				stopDetect(elems[i], eventType, namespace);
			}
			return;
		} // param is string
		else if (elem instanceof Array) {
			for (i=0, len=elem.length; i<len; i++) {
				stopDetect(elem[i], eventType, namespace);
			}
			return;
		} // param is array of elems
		else if (!(elem instanceof HTMLElement)) {
			throw new TypeError('stopDetect() - elem is not an HTMLElement.');
		}
		
		// Process param: namespace
		if (typeof namespace === 'string' && namespace.length > 0) {
			if (!/^[a-zA-Z]+$/.test(namespace)) {
				throw new RangeError('stopDetect() - namespace must be alphabetical. Received: "' + namespace +'"');
			}
			namespace = '.' + namespace; // prepend dot to namespace
		}
		else {
			namespace = '';
		}
		
		// Stop detector for each event type
		var detector;
		var eventTypeArray = eventType.split(' ');
		for (i=0, len=eventTypeArray.length; i<len; i++) {
			detector = getDetection(elem, eventTypeArray[i], namespace);
			
			// DEBUG
			//console.log('cleaning up ' +  elem.id + ' ' + eventTypeArray[i] + ' null? ' +  (detector === null));
			if (detector !== null) {
				detector.cleanup();
				removeDetection(detector);
			}
		}
	} //stopDetect()
	
	/**
	 * Returns number of detectors set up
	 */
	function getDetectorCount() {
		return detectors.length;
	}
	
	function getPanGestureCount() {
		return panGestures.length;
	}
	
	function getPressGestureCount() {
		return pressGestures.length;
	}
	
	function getSwipeGestureCount() {
		return swipeGestures.length;
	}
	
	function getTapGestureCount() {
		return tapGestures.length;
	}
	
	/**
	 * @private
	 * Disables stuff like user-select, touch callout etc. during detector, which may cause undesirable side-effects
	 */
	function disableNativeTouchBehaviour(elem) {
		elem.style.webkitUserSelect = 'none';
		elem.style.mozUserSelect = 'none';
		elem.style.msUserSelect = 'none';
		elem.style.userSelect = 'none';
		elem.style.webkitTouchCallout = 'none';
		elem.style.webkitUserDrag = 'none';
		elem.style.webkitTapHighlightColor = 'rgba(0,0,0,0)';
		elem.style.msTouchSelect = 'none';
		elem.style.msContentZooming = 'none';
		elem.style.msTouchAction = 'none';
		elem.style.touchAction = 'none';
	} //disableNativeTouchBehaviour()
	
	/** 
	 * @private
	 * Clones a copy of default options 
	 */
	function cloneDefaultOptions() {
		var options = {};
		for (var i in DEFAULT_OPTIONS) {
			if (DEFAULT_OPTIONS.hasOwnProperty(i)) {
				options[i] = DEFAULT_OPTIONS[i];
			}
		}
	   return options;
	} // cloneDefaultOptions()
	
	/**
	 * @private
	 * Starts emitting custom events: [panstart, panend] + [panleft, panright, panup, pandown]
	 * @param: {HTMLElement} elem 
	 *         {object} userOptions (optional) - settings to overwrite defaults {panXThreshold, panYThreshold, enableMouse, enableTouch, enablePen}
	 *         {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return: {Detection}
	 */
	function createPanDetection(elem, options, namespace) {
		// Handlers
		function onPanGestureStart(e) {
			// Ignore this start event if a pan gesture already exists. (Don't let multi-touch confuse pan)
			var gestures = findGestures(panGestures, elem);
			if (gestures.length > 0) {
				return;
			}
			
			// Find out touch ID and position
			var touchID, touchStartPosition;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			}
			else if (e.type === 'pointerdown') {
				touchID = e.pointerId;
				touchStartPosition = [e.clientX, e.clientY];
			}
			else {
				// only handle left mouse
				if (e.which !== 1) {
					return;
				}
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			}
			// DEBUG
			//console.log(e.type+' touchID: ' + touchID);
			
			// Create gesture and set up listeners for move/end
			var gesture = {
				element: elem,
				touchID: touchID,
				touchStartPosition: touchStartPosition,
				displacedX: null,
				displacedY: null,
				panStarted: false
			};
			panGestures.push(gesture);
		} // onPanGestureStart()
		
		function onPanGestureMove(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this move event if no pan gestures exist.
			var gestures = findGestures(panGestures, elem);
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchmove') {
				// Find the first touch meant for this elem
				var touch = null;
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						touch = e.changedTouches[i];
						break;
					}
				}
				if (touch === null) {
					// DEBUG
					//console.log('ignoring touchmove event. No matching touchID found');
					return;
				}
				currPosition = [touch.clientX, touch.clientY];
			}
			else if (e.type === 'pointermove') {
				gesture = findGesture(panGestures, elem, e.pointerId);
				currPosition = [e.clientX, e.clientY];
			}
			else {
				gesture = findGesture(panGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			}
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not pan gesture for #' + elem.id);
				return;
			}
			
			// Calculate distance moved
			var displacedX = currPosition[0] - gesture.touchStartPosition[0];
			var displacedY = currPosition[1] - gesture.touchStartPosition[1];
			if (gesture.displacedX === displacedX && gesture.displacedY === displacedY) {
				return;
			} // no movement
			
			// Update displacement
			gesture.displacedX = displacedX;
			gesture.displacedY = displacedY;
			var eventInitDict = {
				'detail': {
					'displacedX': displacedX,
					'displacedY': displacedY
				}
			};
			
			// Emit pan event if threshold exceeded
			var thresholdExceeded = false;
			if (displacedX > options.panXThreshold) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panright'+namespace, eventInitDict));
			} // pan right detected
			else if (0 - displacedX > options.panXThreshold) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panleft'+namespace, eventInitDict));
			} //pan left detected
			if (displacedY > options.panYThreshold) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('pandown'+namespace, eventInitDict));
			} // pan down detected
			else if (0-displacedY > options.panYThreshold) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panup'+namespace, eventInitDict));
			} //pan up detected
			//console.log('thresholdHold exceeded?: ' + thresholdExceeded);
			
			// Send panstart event if this is the first one
			if (thresholdExceeded && !gesture.panStarted) {
				gesture.panStarted = true;
				
				eventInitDict.detail.startX = gesture.touchStartPosition[0];
				eventInitDict.detail.startY = gesture.touchStartPosition[1];
				elem.dispatchEvent(new CustomEvent('panstart'+namespace, eventInitDict));
			}
		} //onPanGestureMove()
		
		function onPanGestureEnd(e) {
			// Ignore this end event if no pan gestures exist.
			var gestures = findGestures(panGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchend') {
				// Find the first touch meant for this elem
				var touch = null;
				for (var i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						touch = e.changedTouches[i];
						break;
					}
				}
				if (touch === null) {
					// DEBUG
					//console.log('ignoring touchend event. No matching touchID found');
					return;
				}
				currPosition = [touch.clientX, touch.clientY];
			} // need to touch event's position from inside touches
			else if (e.type === 'pointerup') {
				gesture = findGesture(panGestures, elem, e.pointerId);
				currPosition = [e.clientX, e.clientY];
			}
			else {
				gesture = findGesture(panGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			}
			
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not pan gesture for #' + elem.id);
				return;
			}
			
			// Emit panend (but only if panstart has previously been emitted)
			if (gesture.panStarted) {
				var eventInitDict = {
					'detail': {
						'displacedX': gesture.displacedX,
						'displacedY': gesture.displacedY,
						'endX': currPosition[0],
						'endY': currPosition[1]
					}
				};
				elem.dispatchEvent(new CustomEvent('panend'+namespace, eventInitDict));
			} // need to send
			removeGesture(panGestures, gesture);
		} //onPanGestureEnd()
		
		function onPanGestureCancel(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this cancel event if no pan gestures exist.
			var gestures = findGestures(panGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event
			var gesture;
			if (e.type === 'touchcancel') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						break;
					}
				}
			} // need to touch event's position from inside touches
			else if (e.type === 'pointercancel') {
				gesture = findGesture(panGestures, elem, e.pointerId);
			}
			else {
				gesture = findGesture(panGestures, elem, null);
			}
			
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not pan gesture for #' + elem.id);
				return;
			}
			removeGesture(panGestures, gesture);
		} //onPanGestureCancel()
		
		// Handlers (Pointer Events) - map pointer events to pan gesture handlers
		function msPanGestureStart(e) {
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onPanGestureStart(e);
			}
		}
		function msPanGestureMove(e) {
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onPanGestureMove(e);
			}
		}
		function msPanGestureEnd(e) {
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onPanGestureEnd(e);
			}
		}
		function msPanGestureCancel(e) {
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onPanGestureCancel(e);
			}
		}
		
		// Setup listeners - needs to be on document as finger may move outside of element's boundary
		function setupPanListeners() {
			if (window.PointerEvent) {
				elem.addEventListener('pointerdown', msPanGestureStart);
				document.addEventListener('pointermove', msPanGestureMove);
				document.addEventListener('pointerup', msPanGestureEnd);
				document.addEventListener('pointercancel', msPanGestureCancel);
			} // for browsers using pointer events (e.g. MS IE)
			else {
				if (options.enableTouch) {
					elem.addEventListener('touchstart', onPanGestureStart);
					document.addEventListener('touchmove', onPanGestureMove);
					document.addEventListener('touchend', onPanGestureEnd);
					document.addEventListener('touchcancel', onPanGestureCancel);
				}
				if (options.enableMouse) {
					elem.addEventListener('mousedown', onPanGestureStart);
					document.addEventListener('mousemove', onPanGestureMove);
					document.addEventListener('mouseup', onPanGestureEnd);
				}
			} // for browsers using touch events (e.g. Chrome)
		} //setupPanListeners()
		
		// Remove event listeners created in the process of detector
		function tearDownPanListeners() {
			if (window.PointerEvent) {
				elem.removeEventListener('pointerdown', msPanGestureStart);
				document.removeEventListener('pointermove', msPanGestureMove);
				document.removeEventListener('pointerup', msPanGestureEnd);
				document.removeEventListener('pointercancel', msPanGestureCancel);
			}
			else {
				if (options.enableTouch) {
					elem.removeEventListener('touchstart', onPanGestureStart);
					document.removeEventListener('touchmove', onPanGestureMove);
					document.removeEventListener('touchend', onPanGestureEnd);
					document.removeEventListener('touchcancel', onPanGestureCancel);
				}
				if (options.enableMouse) {
					elem.removeEventListener('mousedown', onPanGestureStart);
					document.removeEventListener('mousemove', onPanGestureMove);
					document.removeEventListener('mouseup', onPanGestureEnd);
				}
			}
		} //tearDownPanListeners()
		
		setupPanListeners();
		
		return {
			eventType: 'pan',
			element: elem,
			namespace: namespace,
			cleanup: tearDownPanListeners
		};
	} //createPanDetection()
	
	/**
	 * @private
	 * Starts emitting custom events:
	 * - pressdown, pressup
	 */
	function createPressDetection(elem, options, namespace) {		
		// Handlers (Touch Events)
		function onPressGestureStart(e) {			
			// Find out touch ID, position, startTime
			var touchID, touchStartPosition, touchStartTime, timerID;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			}
			else if (e.type === 'pointerdown') {
				touchID = e.pointerId;
				touchStartPosition = [e.clientX, e.clientY];
			}
			else {
				// only handle left mouse
				if (e.which !== 1) {
					return;
				}
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			}
			touchStartTime = Date.now();
			timerID = setTimeout(function(touchID) {
				// DEBUG
				//console.log('timeout at ' + Date.now());
				
				// check if touch is still on the same position
				var eventInitDict = {
					'detail': {
						'clientX': touchStartPosition[0],
						'clientY': touchStartPosition[1]
					}
				};
				elem.dispatchEvent(new CustomEvent('pressdown'+namespace, eventInitDict));
				//console.log('pressdown event dispatched for touchID: ' + touchID);
			}, options.pressDurationThreshold, touchID);
			// DEBUG
			//console.log(e.type+' touchID: ' + touchID + ', startTime: ' + touchStartTime + ', timerID: ' + timerID);
			
			// Create gesture
			pressGestures.push({
				element: elem,
				touchID: touchID,
				touchStartPosition: touchStartPosition,
				touchStartTime: touchStartTime,
				timerID: timerID,
				pressStarted: false,
				pressEndedEarly: false
			});
		} //onPressGestureStart()
		
		function onPressGestureMove(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this move event if no pan gestures exist.
			var gestures = findGestures(pressGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchmove') {
				// Find the first touch meant for this elem
				var touch = null;
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						touch = e.changedTouches[i];
						break;
					}
				}
				if (touch === null) {
					// DEBUG
					//console.log('ignoring touchmove event. No matching touchID found');
					return;
				}
				currPosition = [touch.clientX, touch.clientY];
			}
			else if (e.type === 'pointermove') {
				gesture = findGesture(pressGestures, elem, e.pointerId);
				currPosition = [e.clientX, e.clientY];
			}
			else {
				gesture = findGesture(pressGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			}
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not press gesture for #' + elem.id);
				return;
			}
			
			// Calculate movement
			var displacedX = currPosition[0] - gesture.touchStartPosition[0];
			var displacedY = currPosition[1] - gesture.touchStartPosition[1];
			// DEBUG
			//console.log('displaced ' + displacedX +', '+displacedY);
			
			if (displacedX > options.pressStrayXMax || 0-displacedX > options.pressStrayXMax ||
			    displacedY > options.pressStrayYMax || 0-displacedY > options.pressStrayYMax) {
				// DEBUG
				//console.log('press detector cancelled due to move, touchID: ' + gesture.touchID);
				clearTimeout(gesture.timerID);
				removeGesture(pressGestures, gesture);
			}
		} //onPressGestureMove()
		
		function onPressGestureEnd(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this end event if no press gestures exist.
			var gestures = findGestures(pressGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchend') {
				// Find the first touch meant for this elem
				var touch = null;
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						touch = e.changedTouches[i];
						break;
					}
				}
				if (touch === null) {
					// DEBUG
					//console.log('ignoring touchend event. No matching touchID found');
					return;
				}
				
				currPosition = [touch.clientX, touch.clientY];
			} // need to touch event's position from inside touches
			else if (e.type === 'pointerup') {
				gesture = findGesture(pressGestures, elem, e.pointerId);
				currPosition = [e.clientX, e.clientY];
			}
			else {
				gesture = findGesture(pressGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			}
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not press gesture for #' + elem.id);
				return;
			}
			
			// Send pressup if duration exceeds threshold
			var touchEndTime = Date.now();
			var duration = touchEndTime - gesture.touchStartTime;
			if (duration > options.pressDurationThreshold) {
				var pressEvent = new CustomEvent('pressup'+namespace, {
					'detail': {
						'clientX': currPosition[0],
						'clientY': currPosition[1],
						'duration': duration
					}
				});
				elem.dispatchEvent(pressEvent);
			}
			
			clearTimeout(gesture.timerID);
			removeGesture(pressGestures, gesture);
			// DEBUG
			//console.log('press detector cancelled due to end, touchID: ' + gesture.touchID);
		} //onPressGestureEnd()
		
		function onPressGestureCancel(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this cancel event if no press gestures exist.
			var gestures = findGestures(pressGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event
			var gesture;
			if (e.type === 'touchcancel') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						break;
					}
				}
			} // need to touch event's position from inside touches
			else if (e.type === 'pointercancel') {
				gesture = findGesture(pressGestures, elem, e.pointerId);
			}
			else {
				gesture = findGesture(pressGestures, elem, null);
			}
			
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not pan gesture for #' + elem.id);
				return;
			}
			removeGesture(pressGestures, gesture);
			// DEBUG
			//console.log('press detector cancelled due to ' + e.type + ', touchID: ' + gesture.touchID);
		} //onPressGestureCancel()
		
		// Handlers (Pointer Events) - resolve pointer events to touch event handlers
		function msPressGestureStart(e) {
			//console.log('start ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onPressGestureStart(e);
			}
		}
		function msPressGestureMove(e) {
			//console.log('move ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onPressGestureMove(e);
			}
		}
		function msPressGestureEnd(e) {
			//console.log('end ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onPressGestureEnd(e);
			}
		}
		function msPressGestureCancel(e) {
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onPressGestureCancel(e);
			}
		}
		
		// Add event listeners (needs to be on document as finger may move outside of element's boundary)
		function setUpPressListeners() {
			//console.log('clean up');
			if (window.PointerEvent) {
				elem.addEventListener('pointerdown', msPressGestureStart);
				document.addEventListener('pointermove', msPressGestureMove);
				document.addEventListener('pointerup', msPressGestureEnd);
				document.addEventListener('pointercancel', msPressGestureCancel);
			} // for browsers using pointer events (e.g. MS IE)
			else {
				if (options.enableTouch) {
					elem.addEventListener('touchstart', onPressGestureStart);
					document.addEventListener('touchmove', onPressGestureMove);
					document.addEventListener('touchend', onPressGestureEnd);
					document.addEventListener('touchcancel', onPressGestureCancel);
				}
				
				if (options.enableMouse) {
					elem.addEventListener('mousedown', onPressGestureStart);
					document.addEventListener('mousemove', onPressGestureMove);
					document.addEventListener('mouseup', onPressGestureEnd);
				}
			}
		} //setUpPressListeners()
		
		// Remove event listeners
		function tearDownPressListeners() {
			//console.log('clean up');
			if (window.PointerEvent) {
				elem.removeEventListener('pointerdown', msPressGestureStart);
				document.removeEventListener('pointermove', msPressGestureMove);
				document.removeEventListener('pointerup', msPressGestureEnd);
				document.removeEventListener('pointercancel', msPressGestureCancel);
			} // for browsers using pointer events (e.g. MS IE)
			else {
				if (options.enableTouch) {
					elem.removeEventListener('touchstart', onPressGestureStart);
					document.removeEventListener('touchmove', onPressGestureMove);
					document.removeEventListener('touchend', onPressGestureEnd);
					document.removeEventListener('touchcancel', onPressGestureCancel);
				}
				
				if (options.enableMouse) {
					elem.removeEventListener('mousedown', onPressGestureStart);
					document.removeEventListener('mousemove', onPressGestureMove);
					document.removeEventListener('mouseup', onPressGestureEnd);
				}
			}
		} //tearDownPressListeners()
		
		setUpPressListeners();
		return {
			eventType: 'press',
			element: elem,
			namespace: namespace,
			cleanup: tearDownPressListeners
		};
	} //createPressDetection()
	
	/**
	 * @private
	 * Starts emitting custom events:
	 * - swipeleft, swiperight, swipeup, swipedown
	 */
	function createSwipeDetection(elem, options, namespace) {
		// Handlers (Touch Events)
		function onSwipeGestureStart(e) {
			// Find out touch ID, position, startTime
			var touchID, touchStartPosition, touchStartTime;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			}
			else if (e.type === 'pointerdown') {
				touchID = e.pointerId;
				touchStartPosition = [e.clientX, e.clientY];
			}
			else {
				// only handle left mouse
				if (e.which !== 1) {
					return;
				}
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			}
			touchStartTime = Date.now();
			// DEBUG
			//console.log(e.type+' touchID: ' + touchID + ', startTime: ' + touchStartTime);
			
			// Create gesture
			swipeGestures.push({
				element: elem,
				touchID: touchID,
				touchStartPosition: touchStartPosition,
				touchStartTime: touchStartTime
			});
		} //onSwipeGestureStart()
		
		function onSwipeGestureEnd(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this end event if no swipe gestures exist.
			var gestures = findGestures(swipeGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchend') {
				// Find the first touch meant for this elem
				var touch = null;
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						touch = e.changedTouches[0];
						break;
					}
				}
				if (touch === null) {
					// DEBUG
					//console.log('ignoring touchend event. No matching touchID found');
					return;
				}
				
				currPosition = [touch.clientX, touch.clientY];
			} // need to touch event's position from inside touches
			else if (e.type === 'pointerup') {
				gesture = findGesture(swipeGestures, elem, e.pointerId);
				currPosition = [e.clientX, e.clientY];
			}
			else {
				gesture = findGesture(swipeGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			}
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not swipe gesture for #' + elem.id);
				return;
			}
			var touchEndTime = Date.now();
			
			// Calculate duration, displacement, velocity
			var duration = touchEndTime - gesture.touchStartTime;
			var displacedX = currPosition[0] - gesture.touchStartPosition[0];
			var displacedY = currPosition[1] - gesture.touchStartPosition[1];
			var velocityX = displacedX / duration;
			var velocityY = displacedY / duration;
			
			// Send swipe events (if threshold exceeeded)
			var eventInitDict = {
				'detail': {
					'startX': gesture.touchStartPosition[0],
					'startY': gesture.touchStartPosition[1],
					'endX': currPosition[0],
					'endY': currPosition[1],
					'velocityX': velocityX,
					'velocityY': velocityY,
					'duration': duration
				}
			};
			if (velocityX > options.swipeVelocityXThreshold && displacedX > options.swipeXThreshold) {
				elem.dispatchEvent(new CustomEvent('swiperight'+namespace, eventInitDict));
			}
			
			if (0-velocityX > options.swipeVelocityXThreshold && 0-displacedX > options.swipeXThreshold) {
				elem.dispatchEvent(new CustomEvent('swipeleft'+namespace, eventInitDict));
			}
			
			if (velocityY > options.swipeVelocityYThreshold && displacedY > options.swipeYThreshold) {
				elem.dispatchEvent(new CustomEvent('swipedown'+namespace, eventInitDict));
			}
			
			if (0-velocityY > options.swipeVelocityYThreshold && 0-displacedY > options.swipeYThreshold) {
				elem.dispatchEvent(new CustomEvent('swipeup'+namespace, eventInitDict));
			}
			
			removeGesture(swipeGestures, gesture);
		} //onSwipeGestureEnd()
		
		function onSwipeGestureCancel(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this cancel event if no swipe gestures exist.
			var gestures = findGestures(swipeGestures, elem);
			if (gestures.length === 0) {
				return;

			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			if (e.type === 'touchcancel') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						break;
					}
				}
			} // need to touch event's position from inside touches
			else if (e.type === 'pointercancel') {
				gesture = findGesture(swipeGestures, elem, e.pointerId);
			}
			else {
				gesture = findGesture(swipeGestures, elem, null);
			}
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not swipe gesture for #' + elem.id);
				return;
			}
			removeGesture(swipeGestures, gesture);
		} //onSwipeGestureCancel()
		
		// Handlers (Pointer Events) - resolve pointer events to touch event handlers
		function msSwipeGestureStart(e) {
			//console.log('start ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onSwipeGestureStart(e);
			}
		}
		function msSwipeGestureEnd(e) {
			//console.log('end ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onSwipeGestureEnd(e);
			}
		}
		function msSwipeGestureCancel(e) {
			//console.log('end ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onSwipeGestureCancel(e);
			}
		}
		
		// Add event listeners (needs to be on document as finger may move outside of element's boundary)
		function setUpSwipeListeners() {
			if (window.PointerEvent) {
				elem.addEventListener('pointerdown', msSwipeGestureStart);
				document.addEventListener('pointerup', msSwipeGestureEnd);
				document.addEventListener('pointercancel', msSwipeGestureCancel);
			}
			else {
				if (options.enableTouch) {
					elem.addEventListener('touchstart', onSwipeGestureStart);
					document.addEventListener('touchend', onSwipeGestureEnd);
					document.addEventListener('touchcancel', onSwipeGestureCancel);
				}
				if (options.enableMouse) {
					elem.addEventListener('mousedown', onSwipeGestureStart);
					document.addEventListener('mouseup', onSwipeGestureEnd);
				}
			}
		} //setUpSwipeListeners()
		
		// Remove event bindings created in the process of detector
		function tearDownSwipeListeners() {
			//console.log('clean up');
			if (window.PointerEvent) {
				elem.removeEventListener('pointerdown', msSwipeGestureStart);
				document.removeEventListener('pointerup', msSwipeGestureEnd);
				document.removeEventListener('pointercancel', msSwipeGestureCancel);
			}
			else {
				if (options.enableTouch) {
					elem.removeEventListener('touchstart', onSwipeGestureStart);
					document.removeEventListener('touchend', onSwipeGestureEnd);
					document.removeEventListener('touchcancel', onSwipeGestureCancel);
				}
				if (options.enableMouse) {
					elem.removeEventListener('mousedown', onSwipeGestureStart);
					document.removeEventListener('mouseup', onSwipeGestureEnd);
				}
			}
		} //tearDownSwipeListeners()
		
		setUpSwipeListeners();
		
		return {
			eventType: 'swipe',
			element: elem,
			namespace: namespace,
			cleanup: tearDownSwipeListeners
		};
	} // createSwipeDetection()
	
	function createTapDetection(elem, options, namespace) {
		// Handlers (Touch Events)
		function onTapGestureStart(e) {
			// Find out touch ID, position, start time
			var touchID, touchStartPosition, touchStartTime;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			}
			else if (e.type === 'pointerdown') {
				touchID = e.pointerId;
				touchStartPosition = [e.clientX, e.clientY];
			}
			else {
				// only handle left mouse
				if (e.which !== 1) {
					return;
				}
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			}
			touchStartTime = Date.now();
			// DEBUG
			//console.log(e.type+' touchID: ' + touchID);
			
			// Create gesture
			tapGestures.push({
				element: elem,
				touchID: touchID,
				touchStartPosition: touchStartPosition,
				touchStartTime: touchStartTime
			});
		} //onTapGestureStart()
		
		function onTapGestureMove(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this move event if no tap gestures exist.
			var gestures = findGestures(tapGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchmove') {
				// Find the first touch meant for this elem
				var touch = null;
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						touch = e.changedTouches[i];
						break;
					}
				}
				if (touch === null) {
					// DEBUG
					//console.log('ignoring touchmove event. No matching touchID found');
					return;
				}
				currPosition = [touch.clientX, touch.clientY];
			}
			else if (e.type === 'pointermove') {
				gesture = findGesture(tapGestures, elem, e.pointerId);
				currPosition = [e.clientX, e.clientY];
			}
			else {
				gesture = findGesture(tapGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			}
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not tap gesture for #' + elem.id);
				return;
			}
			
			// Calculate movement
			var displacedX = currPosition[0] - gesture.touchStartPosition[0];
			var displacedY = currPosition[1] - gesture.touchStartPosition[1];
			// DEBUG
			//console.log('displaced ' + displacedX +', '+displacedY);
			
			if (displacedX > options.tapStrayXMax || 0-displacedX > options.tapStrayXMax ||
			    displacedY > options.tapStrayYMax || 0-displacedY > options.tapStrayYMax) {
				// DEBUG
				//console.log('tap detector cancelled due to move, touchID: ' + gesture.touchID);
				removeGesture(tapGestures, gesture);
			}
		} //onTapGestureMove()
		
		function onTapGestureEnd(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this end event if no tap gestures exist.
			var gestures = findGestures(tapGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			var currPosition;
			if (e.type === 'touchend') {
				// Find the first touch meant for this elem
				var touch = null;
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						touch = e.changedTouches[0];
						break;
					}
				}
				if (touch === null) {
					// DEBUG
					//console.log('ignoring touchend event. No matching touchID found');
					return;
				}
				
				currPosition = [touch.clientX, touch.clientY];
			} // need to touch event's position from inside touches
			else if (e.type === 'pointerup') {
				gesture = findGesture(tapGestures, elem, e.pointerId);
				currPosition = [e.clientX, e.clientY];
			}
			else {
				gesture = findGesture(tapGestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			}
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not tap gesture for #' + elem.id);
				return;
			}
			
			// Calculate duration
			var duration = Date.now() - gesture.touchStartTime;
			if (duration < options.tapDurationMax) {
				// Send tap events (if within duration)
				var eventInitDict = {
					'detail': {
						'startX': gesture.touchStartPosition[0],
						'startY': gesture.touchStartPosition[1],
						'endX': currPosition[0],
						'endY': currPosition[1]
					}
				};
				elem.dispatchEvent(new CustomEvent('tap'+namespace, eventInitDict));
			}
			
			removeGesture(tapGestures, gesture);
		} //onTapGestureEnd()
		
		function onTapGestureCancel(e) {
			var i, len; // tmp variables for loops
			
			// Ignore this cancel event if no tap gestures exist.
			var gestures = findGestures(tapGestures, elem);
			if (gestures.length === 0) {
				return;
			}
			
			// Find the gesture for this event, and the event's position
			var gesture;
			if (e.type === 'touchcancel') {
				for (i=0, len=e.changedTouches.length; i<len; i++) {
					//var compareTouchID = createCompareTouchIDFunction(e.changedTouches[i].identifier);
					//var tmp = gestures.filter(compareTouchID);
					var tmp = gestures.filter(function(element) {
						return element.touchID === e.changedTouches[i].identifier;
					});
					if (tmp.length > 0) {
						gesture = tmp[0];
						break;
					}
				}
			} // need to touch event's position from inside touches
			else if (e.type === 'pointercancel') {
				gesture = findGesture(tapGestures, elem, e.pointerId);
			}
			else {
				gesture = findGesture(tapGestures, elem, null);
			}
			if (gesture === null) {
				// DEBUG
				//console.log('ignoring '+e.type+' event. Not tap gesture for #' + elem.id);
				return;
			}
			removeGesture(tapGestures, gesture);
		} //onTapGestureCancel()
		
		// Handlers (Pointer Events) - resolve pointer events to touch event handlers
		function msTapGestureStart(e) {
			//console.log('start ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onTapGestureStart(e);
			}
		}
		function msTapGestureMove(e) {
			//console.log('move ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onTapGestureMove(e);
			}
		}
		function msTapGestureEnd(e) {
			//console.log('end ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onTapGestureEnd(e);
			}
		}
		function msTapGestureCancel(e) {
			//console.log('end ' + e.pointerType);
			if (options.enableTouch && e.pointerType === 'touch' ||
				options.enableMouse && e.pointerType === 'mouse' ||
				options.enablePen && e.pointerType === 'pen') {
				onTapGestureCancel(e);
			}
		}
		
		// Add event listeners (needs to be on document as finger may move outside of element's boundary)
		function setUpTapListeners() {
			if (window.PointerEvent) {
				elem.addEventListener('pointerdown', msTapGestureStart);
				document.addEventListener('pointermove', msTapGestureMove);
				document.addEventListener('pointerup', msTapGestureEnd);
				document.addEventListener('pointercancel', msTapGestureCancel);
			}
			else {
				if (options.enableTouch) {
					elem.addEventListener('touchstart', onTapGestureStart);
					document.addEventListener('touchmove', onTapGestureMove);
					document.addEventListener('touchend', onTapGestureEnd);
					document.addEventListener('touchcancel', onTapGestureCancel);
				}

				// Some browsers emulates mouse events on touchstart/touchend
				// If they support touch events, we should not enable mouse
				if (options.enableMouse && !('ontouchstart' in document.documentElement)) {
					elem.addEventListener('mousedown', onTapGestureStart);
					document.addEventListener('mousemove', onTapGestureMove);
					document.addEventListener('mouseup', onTapGestureEnd);
				}
			}
		} //setUpTapListeners()
		
		// Remove event bindings created in the process of detector
		function tearDownTapListeners() {
			//console.log('clean up');
			if (window.PointerEvent) {
				elem.removeEventListener('pointerdown', msTapGestureStart);
				document.removeEventListener('pointermove', msTapGestureMove);
				document.removeEventListener('pointerup', msTapGestureEnd);
				document.removeEventListener('pointercancel', msTapGestureCancel);
			}
			else {
				if (options.enableTouch) {
					elem.removeEventListener('touchstart', onTapGestureStart);
					document.removeEventListener('touchmove', onTapGestureMove);
					document.removeEventListener('touchend', onTapGestureEnd);
					document.removeEventListener('touchcancel', onTapGestureCancel);
				}
				
				// See above (setUpTapListeners)
				if (options.enableMouse) {
					elem.removeEventListener('mousedown', onTapGestureStart);
					document.removeEventListener('mousemove', onTapGestureMove);
					document.removeEventListener('mouseup', onTapGestureEnd);
				}
			}
		} //tearDownTapListeners()
		
		setUpTapListeners();
		
		return {
			eventType: 'tap',
			element: elem,
			namespace: namespace,
			cleanup: tearDownTapListeners
		};
	} //createTapDetection()
		
	/**
	 * Gets distance between 2 points
	 * @param: {Array, Array} two points of [x,y]
	 * @return: {number}
	 */
	 /* TODO: Resurrect when we enable pinch
	function getDistance(p1, p2) {
		var x = p1[0] - p2[0];
		var y = p1[1] - p2[1];
		return Math.sqrt(x*x + y*y);
	} //getDistance()
	*/
	
	/**
	 * @private
	 * Checks if a detector for an element is already set up
	 * @param: {HTMLElement, string, string}
	 * @return: {bool}
	 */
	function isDetecting(elem, eventType, namespace) {
		var i, len; // tmp variables for loops
		for (i=0, len=detectors.length; i<len; i++) {
			var detector = detectors[i];
			if (detector.element === elem && detector.eventType === eventType && detector.namespace === namespace) {
				return true;
			}
		}
		return false;
	} //isDetecting()
			
	/**
	 * @private
	 * Gets a Detection object
	 * @param: {HTMLElement, string, string}
	 * @return: {Detection | null}
	 */
	function getDetection(elem, eventType, namespace) {
		var i, len; // tmp variables for loops
		for (i=0, len=detectors.length; i<len; i++) {
			var detector = detectors[i];
			if (detector.element === elem && detector.eventType === eventType && detector.namespace === namespace) {
				return detector;
			}
		}
		return null;
	} //getDetection()
	
	/**
	 * @private
	 * Removes a detector
	 */
	function removeDetection(detectorToRemove) {
		var i, len; // tmp variables for loops
		for (i=0, len=detectors.length; i<len; i++) {
			var detector = detectors[i];
			if (detector.element === detectorToRemove.element && detector.eventType === detectorToRemove.eventType && detector.namespace === detectorToRemove.namespace) {
				detectors.splice(i, 1);
				return;
			}
		}
	} //removeDetection()
	
	/**
	 * @private
	 * Finds a Gesture by its element and touchID
	 * @param: {Array} gestures - the list of gestures to search from, e.g. panGestures
	 *         {HTMLElement} elem
	 *         {number} touchID
	 * @return: {Gesture | null}
	 */
	function findGesture(gestures, elem, touchID) {
		var i, len; // tmp variables for loops
		for (i=0, len=gestures.length; i<len; i++) {
			var gesture = gestures[i];
			if (gesture.element === elem && gesture.touchID === touchID) {
				return gesture;
			}
		}
		return null;
	} //findGesture()
	
	/**
	 * @private
	 * Finds Gestures by a element
	 * @param: {Array} gestures - the list of gestures to search from, e.g. panGestures
	 *         {HTMLElement} elem
	 * @return {Array}
	 */
	function findGestures(gestures, elem) {
		return gestures.filter(function(gesture){
			return gesture.element === elem;
		});
	} //findGestures()
	
	/**
	 * @private
	 * Removes a Gesture from its registry
	 * @param: {Array} gestures - the list of gestures to search from, e.g. panGestures
	 *         {Gesture} object to remove
	 */
	function removeGesture(gestures, gestureToRemove) {
		var i, len; // tmp variables for loops
		for (i=0, len=gestures.length; i<len; i++) {
			var gesture = gestures[i];
			if (gesture.element === gestureToRemove.element && gesture.touchID === gestureToRemove.touchID) {
				gestures.splice(i, 1);
				return;
			}
		}
	} //removeGesture()
	
	global.tove = {
		detect: detect,
		stopDetect: stopDetect,
		getDetectorCount: getDetectorCount,
		getPanGestureCount: getPanGestureCount,
		getPressGestureCount: getPressGestureCount,
		getSwipeGestureCount: getSwipeGestureCount,
		getTapGestureCount: getTapGestureCount
	};
})(this);
