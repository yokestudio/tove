/***
 * Library - JS Custom Touch Events
 *
 * Detects custom touch events and emits as DOM CustomEvents
 *
 * Event Types:
 * [pan]: mouse/touch begins on target element, and moves to anywhere in the document.
 *   - [panstart, panend] triggered at beginning and end of the move
 *   - [panleft, panright, panup, pandown] triggered at any point during the move, if mouse/touch is displaced more than the threshold
 *
 * [press]: mouse/touch begins on target element, held for a period of time.
 *   - [pressdown] triggered if held down for sufficient time, a pressdown event is triggered.
 *   - [pressup] triggered on release
 *   - at any point, if significant movement is detected, the press gesture is cancelled, i.e. pressup will not be triggered
 *
 * [tap]: mouse/touch begins on a target element, and released quickly.
 *   - [tap] triggered if released before threshold duration, and without movement
 *   - if held down for a signficant amount of time, the tap gesture is cancelled.
 *   - at any point, if significant movement is detected, the tap gesture is cancelled.
 *
 * [swipe]: mouse/touch begins on target element, moves, and ends anywhere in the document.
 *   - [swipeleft, swiperight, swipeup, swipedown] triggered if displacement within time exceeds a certain velocity in the x-axis or y-axis.
 *
 * [pinch]: 2 touches begin on target element, move and end anywhere in the document.
 *   - [pinchstart, pinchend] triggered at beginning and end of the move
 *   - [pinchin, pinchout] triggered if distance between the 2 points decreases / increases.
 *   - Note: no mouse support
 *   - Note: only 1 pinch gesture is allowed per element
 *
 * [TODO rotate]:
 *   - [rotatestart, rotateend] triggered at beginning and end of the move
 *
 * Dependencies: support for DOM CustomEvent
 ***/
(function(global) {
	'use strict';

	let ALL_EVENT_TYPES = 'pan press swipe tap dbltap pinch rotate';
	let DEFAULT_OPTIONS = {
		// pan
		'panXThreshold': 10, // min px required for a pan in x-axis
		'panYThreshold': 10, // min px required for a pan in y-axis

		// press
		'pressStrayXMax': 20, // max px of stray allowed after touch starts, before touch ends
		'pressStrayYMax': 20,
		'pressDurationThreshold': 400, // min duration (ms) required for a pressdown

		// tap
		'tapStrayXMax': 10, // max px of stray allowed after touch starts, before touch ends
		'tapStrayYMax': 10,
		'tapDurationMax': 450, // max duration (ms) for a tap

		// dbltap
		'dbltapIntervalMax': 400,
		'dbltapStrayXMax': 20,
		'dbltapStrayYMax': 20,

		// swipe
		'swipeXThreshold': 16, // min px required for a swipe in x-axis
		'swipeYThreshold': 16, // min px required for a swipe in y-axis
		'swipeVelocityXThreshold': 0.05, // min velocity in x-axis required for swipeleft, swiperight (px per ms)
		'swipeVelocityYThreshold': 0.05, // min velocity in y-axis required for swipeup, swipedown (px per ms)

		'pinchDistanceThreshold': 1.4,

		// global
		'enableMouse': true,
		'enableTouch': true,

		'touchAction': 'none' // prevents browser from handling panning (scrolling) and pinching actions (zooming)
	};
	let EVENT_LISTENER_PARAM = supportsPassiveEventListeners() ? {'passive': true} : false;

	// each detector is {element: <HTMLElement>, eventType: <string>, namespace: <string>, and cleanup: <function>}
	let detectors = [];

	// all currently ongoing gestures
	let pan_gestures = [];
	let press_gestures = [];
	let swipe_gestures = [];
	let tap_gestures = [];
	let dbltap_gestures = [];
	let pinch_gestures = [];
	let rotate_gestures = [];

	/**
	 * Starts emitting custom events based on the type enterd
	 * - Note: native touch behaviour will be disabled
	 * - Note: an element can have two detections on different namespaces.
	 * @param {HTMLElement | string} elem - if string, querySelectorAll will be applied to all elements matching the string
	 *        {string} event_type - space-separated types of events [pan, press, swipe]
	 *        {object} user_options (optional) - will be merged into default options
	 *        {string} namespace (optional)
	 */
	function detect(elem, event_type, user_options, namespace) {
		// Process param: event_type
		if (typeof event_type !== 'string') {
			throw new TypeError('detect() - event_type must be string');
		}
		if (event_type.trim() === '') {
			return;
		}
		if (event_type.trim() === 'all') {
			event_type = ALL_EVENT_TYPES; /* eslint-disable-line no-param-reassign */
		}

		// Process param: elem - find the HTMLElement(s)
		if (typeof elem === 'string') {
			let elems = document.querySelectorAll(elem);
			if (elems.length === 0) {
				console.warn('detect() - no elements found matching: ' + elem);
				return;
			}

			for (let i = 0, len = elems.length; i < len; i++) {
				detect(elems[i], event_type, user_options, namespace);
			}
			return;
		} // param is string
		if (elem instanceof Array) {
			for (let i = 0, len = elem.length; i < len; i++) {
				detect(elem[i], event_type, user_options, namespace);
			}
			return;
		} // param is array of elems
		if (!(elem instanceof HTMLElement)) {
			throw new TypeError('detect() - elem is not an HTMLElement.');
		}

		// Process param: user_options
		let options = Object.assign({}, DEFAULT_OPTIONS); // make a clone
		if (typeof user_options === 'object') {
			for (let key in options) {
				if (options.hasOwnProperty(key) && typeof user_options[key] === typeof options[key]) {
					options[key] = user_options[key];
				} // update only if compatible
			} // go through each option setting
		}

		// Process param: namespace
		if (typeof namespace === 'string' && namespace.length > 0) {
			if (!(/^[a-zA-Z]+$/u).test(namespace)) {
				throw new RangeError('detect() - namespace must be alphabetical. Received: "' + namespace + '"');
			}
			namespace = '.' + namespace; /* eslint-disable-line no-param-reassign */
		}
		else {
			namespace = ''; /* eslint-disable-line no-param-reassign */
		}

		// Parse all event types
		let event_types = event_type.split(' ');
		event_types = event_types.filter(function(item, pos) { // remove duplicates and empty
			return item !== '' && event_types.indexOf(item) === pos;
		});

		// Start detection for each event type
		let detector; // to be created for each event
		for (let i = 0, len = event_types.length; i < len; i++) {
			switch (event_types[i]) {
				case 'pan':
					if (!isDetecting(elem, 'pan', namespace)) {
						detector = createPanDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;

				case 'press':
					if (!isDetecting(elem, 'press', namespace)) {
						detector = createPressDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				case 'swipe':
					if (!isDetecting(elem, 'swipe', namespace)) {
						detector = createSwipeDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;

				case 'tap':
					if (!isDetecting(elem, 'tap', namespace)) {
						detector = createTapDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;

				case 'dbltap':
					if (!isDetecting(elem, 'dbltap', namespace)) {
						detector = createDblTapDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;

				case 'pinch':
					if (!isDetecting(elem, 'pinch', namespace)) {
						detector = createPinchDetection(elem, options, namespace);
						detectors.push(detector);
					}
					break;
				default:
					console.warn('detect() - unrecognised event_type ' + event_types[i]);
					break;
			} // switch
		} // for each event type

		// Override native touch behaviour
		elem['style'].webkitUserSelect = 'none';
		elem['style'].mozUserSelect = 'none';
		elem['style'].msUserSelect = 'none';
		elem['style'].userSelect = 'none';
		elem['style'].webkitTouchCallout = 'none';
		elem['style'].webkitUserDrag = 'none';
		elem['style'].userDrag = 'none';
		elem['style'].webkitTapHighlightColor = 'rgba(0,0,0,0)';
		elem['style'].msTouchSelect = 'none';
		elem['style'].msContentZooming = 'none';
		elem['style'].msTouchAction = options['touchAction'];
		elem['style'].touchAction = options['touchAction'];
		if (event_types.includes('pinch') && options['touchAction'].includes('pinch-zoom')) {
			console.warn('Pinch-zoom touch-action is still enabled. Pinch detection may not work correctly.');
		}

		// Always prevent drag to ensure element doesn't move around during detection of gestures
		elem.addEventListener('dragstart', preventDefault);
	} // detect()

	/**
	 * Stops detecting custom events
	 * @param {HTMLElement | string} elem - if string, querySelectorAll will be applied to all elements matching the string
	 *        {string} event_type - space-separated types of events [pan, press, swipe]
	 *        {string} namespace (optional)
	 */
	function stopDetect(elem, event_type, namespace) {
		// Process param: event_type
		if (typeof event_type !== 'string') {
			throw new TypeError('stopDetect() - type must be string');
		}
		if (event_type.trim() === 'all') {
			event_type = ALL_EVENT_TYPES; /* eslint-disable-line no-param-reassign */
		}

		// Process param: elem - find the HTMLElement
		if (typeof elem === 'string') {
			let elems = document.querySelectorAll(elem);
			if (elems.length === 0) {
				console.warn('stopDetect() - no elements found matching: ' + elem);
				return;
			}

			for (let i = 0, len = elems.length; i < len; i++) {
				stopDetect(elems[i], event_type, namespace);
			}
			return;
		} // param is string
		if (elem instanceof Array) {
			for (let i = 0, len = elem.length; i < len; i++) {
				stopDetect(elem[i], event_type, namespace);
			}
			return;
		} // param is array of elems
		if (!(elem instanceof HTMLElement)) {
			throw new TypeError('stopDetect() - elem is not an HTMLElement.');
		}

		// Process param: namespace
		if (typeof namespace === 'string' && namespace.length > 0) {
			if (!(/^[a-zA-Z]+$/u).test(namespace)) {
				throw new RangeError('stopDetect() - namespace must be alphabetical. Received: "' + namespace + '"');
			}
			namespace = '.' + namespace; /* eslint-disable-line no-param-reassign */
		}
		else {
			namespace = ''; /* eslint-disable-line no-param-reassign */
		}

		// Parse all event types
		let event_types = event_type.split(' ');
		event_types = event_types.filter(function(item, pos) { // remove duplicates and empty
			return item !== '' && event_types.indexOf(item) === pos;
		});

		// Stop detector for each event type
		for (let i = 0, len = event_types.length; i < len; i++) {
			// detector = getDetector(elem, event_types[i], namespace);
			let detector = detectors.find(function(v) {
				return v['element'] === elem && v['eventType'] === event_types[i] && v['namespace'] === namespace;
			});
			if (!detector) {
				continue;
			}

			// console.debug('cleaning up ' +  elem.id + ' ' + event_types[i] + ' null? ' +  (detector === null));
			detector.cleanup();
			removeDetector(detector);
		}

		elem.removeEventListener('dragstart', preventDefault);
	} // stopDetect()

	/**
	 * @return {array} of detector objects
	 */
	function getDetectors() {
		return detectors;
	} // getDetectors()

	/**
	 * @return {number} of detectors set up
	 */
	function getDetectorCount() {
		return detectors.length;
	} // getDetectorCount()

	/**
	 * @return {number} of pan gestures currently being tracked
	 */
	function getPanGestureCount() {
		return pan_gestures.length;
	} // getPanGestureCount()

	/**
	 * @return {number} of press gestures currently being tracked
	 */
	function getPressGestureCount() {
		return press_gestures.length;
	} // getPressGestureCount()

	/**
	 * @return {number} of swipe gestures currently being tracked
	 */
	function getSwipeGestureCount() {
		return swipe_gestures.length;
	} // getSwipeGestureCount()

	/**
	 * @return {number} of tap gestures currently being tracked
	 */
	function getTapGestureCount() {
		return tap_gestures.length;
	} // getTapGestureCount()

	function getDbltapGestureCount() {
		return dbltap_gestures.length;
	} // getDbltapGestureCount()

	/**
	 * @return {number} of pinch gestures currently being tracked
	 */
	function getPinchGestureCount() {
		return pinch_gestures.length;
	} // getPinchGestureCount()

	/**
	 * @return {number} of rotate gestures currently being tracked
	 */
	function getRotateGestureCount() {
		return rotate_gestures.length;
	} // getRotateGestureCount()

/****************
*   INTERNALS   *
*****************/
	/**
	 * @private
	 * Checks if the browser supports passive event listeners (e.g. Chrome 58+)
	 * @return {boolean}
	 */
	function supportsPassiveEventListeners() {
		let support = false;
		try {
			let opts = Object.defineProperty({}, 'passive', {
				'get': function() {
					support = true;
				}
			});

			global.addEventListener('test', null, opts);
		}
		catch (e) {
			// do nothing
		}

		return support;
	} // supportsPassiveEventListeners()

	/**
	 * @private
	 * Used for multiple listeners
	 */
	function preventDefault(e) {
		e.preventDefault();
	} // preventDefault()

	/**
	 * @private
	 * Starts emitting custom events: [panstart, panend] + [panleft, panright, panup, pandown]
	 * @param {HTMLElement} elem
	 *        {object} options (optional) - {panXThreshold, panYThreshold, enableMouse, enableTouch}
	 *        {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createPanDetection(elem, options, namespace) {
		function onPanGestureStart(e) {
			// Ignore this event if a pan gesture already exists for this element. (Don't let multi-touch confuse pan)
			let gestures = pan_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length > 0) {
				return;
			}

			// Find out touch ID and position
			let touchID, touchStartPosition;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				// only handle left mouse
				if (e.which !== 1) {
					return;
				}
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse

			// Create gesture
			pan_gestures.push({
				'element': elem,
				'touchID': touchID,
				'touchStartPosition': touchStartPosition,
				'displacedX': null,
				'displacedY': null,
				'panStarted': false
			});
		} // onPanGestureStart()

		function onPanGestureMove(e) {
			// Ignore this event if no pan gestures exist
			let gestures = pan_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture, currPosition;
			if (e.type === 'touchmove') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(pan_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				} // for all changed touches
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(pan_gestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}

			// Calculate distance moved
			let displacedX = currPosition[0] - gesture['touchStartPosition'][0];
			let displacedY = currPosition[1] - gesture['touchStartPosition'][1];
			if (gesture['displacedX'] === displacedX && gesture['displacedY'] === displacedY) {
				return;
			} // no movement

			// Update displacement
			gesture['displacedX'] = displacedX;
			gesture['displacedY'] = displacedY;
			let event_dict = {
				'detail': {
					'displacedX': displacedX,
					'displacedY': displacedY
				}
			};

			// Emit pan event if threshold exceeded
			let thresholdExceeded = false;
			if (displacedX > options['panXThreshold']) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panright' + namespace, event_dict));
			} // pan right detected
			else if (0 - displacedX > options['panXThreshold']) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panleft' + namespace, event_dict));
			} // pan left detected
			if (displacedY > options['panYThreshold']) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('pandown' + namespace, event_dict));
			} // pan down detected
			else if (0 - displacedY > options['panYThreshold']) {
				thresholdExceeded = true;
				elem.dispatchEvent(new CustomEvent('panup' + namespace, event_dict));
			} // pan up detected
			// console.debug('thresholdHold exceeded?: ' + thresholdExceeded);

			// Send panstart event if this is the first one
			if (thresholdExceeded && !gesture['panStarted']) {
				gesture['panStarted'] = true;

				event_dict.detail.startX = gesture['touchStartPosition'][0];
				event_dict.detail.startY = gesture['touchStartPosition'][1];
				elem.dispatchEvent(new CustomEvent('panstart' + namespace, event_dict));
			}
		} // onPanGestureMove()

		function onPanGestureEnd(e) {
			// Ignore this event if no pan gestures exist
			let gestures = pan_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture, currPosition;
			if (e.type === 'touchend') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(pan_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(pan_gestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}

			// Emit panend (but only if panstart has previously been emitted)
			if (gesture['panStarted']) {
				elem.dispatchEvent(new CustomEvent('panend' + namespace, {
					'detail': {
						'displacedX': gesture['displacedX'],
						'displacedY': gesture['displacedY'],
						'endX': currPosition[0],
						'endY': currPosition[1]
					}
				}));
			}
			removeGesture(pan_gestures, gesture);
		} // onPanGestureEnd()

		function onPanGestureCancel(e) {
			// Ignore this event if no pan gestures exist
			let gestures = pan_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event
			let gesture;
			if (e.type === 'touchcancel') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(pan_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(pan_gestures, elem, null);
			} // mouse
			if (!gesture) {
				return;
			}

			removeGesture(pan_gestures, gesture);
		} // onPanGestureCancel()

		function setUpPanListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onPanGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onPanGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onPanGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onPanGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.addEventListener('mousedown', onPanGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mousemove', onPanGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onPanGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // setUpPanListeners()
		function tearDownPanListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onPanGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onPanGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onPanGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onPanGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onPanGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mousemove', onPanGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onPanGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // tearDownPanListeners()

		setUpPanListeners();
		return {
			'eventType': 'pan',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownPanListeners
		};
	} // createPanDetection()

	/**
	 * @private
	 * Starts emitting custom events: [pressdown, pressup]
	 * @param {HTMLElement} elem
	 *        {object} options (optional) - {pressDurationThreshold, pressStrayXMax, pressStrayYMax, enableMouse, enableTouch}
	 *        {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createPressDetection(elem, options, namespace) {
		function onPressGestureStart(e) {
			let touchID, touchStartPosition, touchStartTime, timerID;

			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				if (e.which !== 1) {
					return;
				} // left mouse
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse
			touchStartTime = Date.now();
			timerID = setTimeout(function() {
				// console.debug('timeout at ' + Date.now());

				elem.dispatchEvent(new CustomEvent('pressdown' + namespace, {
					'detail': {
						'clientX': touchStartPosition[0],
						'clientY': touchStartPosition[1]
					}
				}));
			}, options['pressDurationThreshold']);
			// console.debug(e.type+' touchID: ' + touchID + ', startTime: ' + touchStartTime + ', timerID: ' + timerID);

			// Create gesture
			press_gestures.push({
				'element': elem,
				'touchID': touchID,
				'touchStartPosition': touchStartPosition,
				'touchStartTime': touchStartTime,
				'timerID': timerID
			});
		} // onPressGestureStart()

		function onPressGestureMove(e) {
			// Ignore this move event if no pan gestures exist
			let gestures = press_gestures.filter(function(g) {
				return g.element === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture;
			let currPosition;
			if (e.type === 'touchmove') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(press_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(press_gestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}

			// Calculate movement
			let displacedX = currPosition[0] - gesture['touchStartPosition'][0];
			let displacedY = currPosition[1] - gesture['touchStartPosition'][1];

			if (displacedX > options['pressStrayXMax'] || 0 - displacedX > options['pressStrayXMax'] ||
			    displacedY > options['pressStrayYMax'] || 0 - displacedY > options['pressStrayYMax']) {
				// console.debug('press detector cancelled due to move, touchID: ' + gesture['touchID']);
				clearTimeout(gesture['timerID']);
				removeGesture(press_gestures, gesture);
			}
		} // onPressGestureMove()

		function onPressGestureEnd(e) {
			// Ignore this end event if no press gestures exist
			let gestures = press_gestures.filter(function(g) {
				return g.element === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture;
			let currPosition;
			if (e.type === 'touchend') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(press_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(press_gestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}

			// Emit "pressup" if duration exceeds threshold
			let duration = Date.now() - gesture['touchStartTime'];
			if (duration > options['pressDurationThreshold']) {
				elem.dispatchEvent(new CustomEvent('pressup' + namespace, {
					'detail': {
						'clientX': currPosition[0],
						'clientY': currPosition[1],
						'duration': duration
					}
				}));
			}

			clearTimeout(gesture['timerID']);
			removeGesture(press_gestures, gesture);
		} // onPressGestureEnd()

		function onPressGestureCancel(e) {
			// Ignore this cancel event if no press gestures exist
			let gestures = press_gestures.filter(function(g) {
				return g.element === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event
			let gesture;
			if (e.type === 'touchcancel') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(press_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(press_gestures, elem, null);
			} // mouse
			if (!gesture) {
				return;
			}

			removeGesture(press_gestures, gesture);
			// console.debug('press detector cancelled due to ' + e.type + ', touchID: ' + gesture['touchID']);
		} // onPressGestureCancel()

		function setUpPressListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onPressGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onPressGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onPressGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onPressGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.addEventListener('mousedown', onPressGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mousemove', onPressGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onPressGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // setUpPressListeners()
		function tearDownPressListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onPressGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onPressGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onPressGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onPressGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onPressGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mousemove', onPressGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onPressGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // tearDownPressListeners()

		setUpPressListeners();
		return {
			'eventType': 'press',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownPressListeners
		};
	} // createPressDetection()

	/**
	 * @private
	 * Starts emitting custom events: [swipeleft, swiperight, swipeup, swipedown]
	 * @param {HTMLElement} elem
	 *        {object} options (optional) - {
	 *                                         swipeXThreshold, swipeYThreshold,
	 *                                         swipeVelocityXThreshold, swipeVelocityYThreshold,
	 *                                         enableMouse, enableTouch
	 *                                      }
	 *        {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createSwipeDetection(elem, options, namespace) {
		function onSwipeGestureStart(e) {
			// Find out touch ID, position, startTime
			let touchID, touchStartPosition, touchStartTime;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				if (e.which !== 1) {
					return;
				} // left mouse
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse
			touchStartTime = Date.now();
			// console.debug(e.type+' touchID: ' + touchID + ', startTime: ' + touchStartTime);

			// Create gesture
			swipe_gestures.push({
				'element': elem,
				'touchID': touchID,
				'touchStartPosition': touchStartPosition,
				'touchStartTime': touchStartTime
			});
		} // onSwipeGestureStart()

		function onSwipeGestureEnd(e) {
			// Ignore this event if no swipe gestures exist
			let gestures = swipe_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture, currPosition;
			if (e.type === 'touchend') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(swipe_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(swipe_gestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}

			// Calculate duration, displacement, velocity
			let duration = Date.now() - gesture['touchStartTime'];
			let displacedX = currPosition[0] - gesture['touchStartPosition'][0];
			let displacedY = currPosition[1] - gesture['touchStartPosition'][1];
			let velocityX = displacedX / duration;
			let velocityY = displacedY / duration;

			// Send swipe events (if threshold exceeeded)
			let event_dict = {
				'detail': {
					'startX': gesture['touchStartPosition'][0],
					'startY': gesture['touchStartPosition'][1],
					'endX': currPosition[0],
					'endY': currPosition[1],
					'velocityX': velocityX,
					'velocityY': velocityY,
					'duration': duration
				}
			};
			if (velocityX > options['swipeVelocityXThreshold'] && displacedX > options['swipeXThreshold']) {
				elem.dispatchEvent(new CustomEvent('swiperight' + namespace, event_dict));
			}

			if (0 - velocityX > options['swipeVelocityXThreshold'] && 0 - displacedX > options['swipeXThreshold']) {
				elem.dispatchEvent(new CustomEvent('swipeleft' + namespace, event_dict));
			}

			if (velocityY > options['swipeVelocityYThreshold'] && displacedY > options['swipeYThreshold']) {
				elem.dispatchEvent(new CustomEvent('swipedown' + namespace, event_dict));
			}

			if (0 - velocityY > options['swipeVelocityYThreshold'] && 0 - displacedY > options['swipeYThreshold']) {
				elem.dispatchEvent(new CustomEvent('swipeup' + namespace, event_dict));
			}

			removeGesture(swipe_gestures, gesture);
		} // onSwipeGestureEnd()

		function onSwipeGestureCancel(e) {
			// Ignore this cancel event if no swipe gestures exist
			let gestures = swipe_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event
			let gesture;
			if (e.type === 'touchcancel') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(swipe_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(swipe_gestures, elem, null);
			} // mouse
			if (!gesture) {
				return;
			}

			removeGesture(swipe_gestures, gesture);
		} // onSwipeGestureCancel()

		function setUpSwipeListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onSwipeGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onSwipeGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onSwipeGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.addEventListener('mousedown', onSwipeGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onSwipeGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // setUpSwipeListeners()
		function tearDownSwipeListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onSwipeGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onSwipeGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onSwipeGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onSwipeGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onSwipeGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // tearDownSwipeListeners()

		setUpSwipeListeners();

		return {
			'eventType': 'swipe',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownSwipeListeners
		};
	} // createSwipeDetection()

	/**
	 * @private
	 * Starts emitting custom events: [tap]
	 * @param {HTMLElement} elem
	 *         {object} options (optional) - {tapDurationThreshold, tapStrayXMax, tapStrayYMax, enableMouse, enableTouch}
	 *         {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createTapDetection(elem, options, namespace) {
		// Handlers (Touch Events)
		function onTapGestureStart(e) {
			// Find out touch ID, position, start time
			let touchID, touchStartPosition, touchStartTime;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				if (e.which !== 1) {
					return;
				} // left mouse
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse
			touchStartTime = Date.now();
			// console.debug(e.type+' touchID: ' + touchID);

			// Create gesture
			tap_gestures.push({
				'element': elem,
				'touchID': touchID,
				'touchStartPosition': touchStartPosition,
				'touchStartTime': touchStartTime
			});
		} // onTapGestureStart()

		function onTapGestureMove(e) {
			// Ignore this move event if no tap gestures exist
			let gestures = tap_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture, currPosition;
			if (e.type === 'touchmove') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(tap_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(tap_gestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}

			// Calculate movement and remove gesture if it strayed too far
			let displacedX = currPosition[0] - gesture['touchStartPosition'][0];
			let displacedY = currPosition[1] - gesture['touchStartPosition'][1];
			if (displacedX > options['tapStrayXMax'] || 0 - displacedX > options['tapStrayXMax'] ||
			    displacedY > options['tapStrayYMax'] || 0 - displacedY > options['tapStrayYMax']) {
				// console.debug('tap detector cancelled due to move, touchID: ' + gesture['touchID']);
				removeGesture(tap_gestures, gesture);
			}
		} // onTapGestureMove()

		function onTapGestureEnd(e) {
			// Ignore this end event if no tap gestures exist
			let gestures = tap_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture, currPosition;
			if (e.type === 'touchend') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(tap_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(tap_gestures, elem, null);
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}

			// Emit "tap" if touch duration is within threshold
			let duration = Date.now() - gesture['touchStartTime'];
			if (duration < options['tapDurationMax']) {
				elem.dispatchEvent(new CustomEvent('tap' + namespace, {
					'detail': {
						'startX': gesture['touchStartPosition'][0],
						'startY': gesture['touchStartPosition'][1],
						'endX': currPosition[0],
						'endY': currPosition[1]
					}
				}));
			}

			removeGesture(tap_gestures, gesture);
		} // onTapGestureEnd()

		function onTapGestureCancel(e) {
			// Ignore this cancel event if no tap gestures exist
			let gestures = tap_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture;
			if (e.type === 'touchcancel') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(tap_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(tap_gestures, elem, null);
			} // mouse
			if (!gesture) {
				return;
			}

			removeGesture(tap_gestures, gesture);
		} // onTapGestureCancel()

		function setUpTapListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onTapGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onTapGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onTapGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onTapGestureCancel, EVENT_LISTENER_PARAM);
			}

			// Some browsers emulates mouse events on touchstart/touchend
			// If they support touch events, we should not enable mouse
			if (options['enableMouse'] && !('ontouchstart' in document.documentElement)) {
				elem.addEventListener('mousedown', onTapGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mousemove', onTapGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onTapGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // setUpTapListeners()
		function tearDownTapListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onTapGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onTapGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onTapGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onTapGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onTapGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mousemove', onTapGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onTapGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // tearDownTapListeners()

		setUpTapListeners();

		return {
			'eventType': 'tap',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownTapListeners
		};
	} // createTapDetection()

	/**
	 * @private
	 * Starts emitting custom events: [dbltap]
	 * @param {HTMLElement} elem
	 *         {object} options (optional) -
	 *                            {
	 *                              tapDurationThreshold, tapStrayXMax, tapStrayYMax,
	 *                              dbltapIntervalMax, dbltapStrayXMax, dbltapStrayYMax,
	 *                              enableMouse, enableTouch
	 *                            }
	 *         {string} namespace (optional) - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createDblTapDetection(elem, options, namespace) {
		// Handlers (Touch Events)
		function onDbltapGestureStart(e) {
			// Find out touch ID, position, start time
			let touchID, touchStartPosition, touchStartTime;
			if (e.type === 'touchstart') {
				touchID = e.changedTouches[0].identifier;
				touchStartPosition = [e.changedTouches[0].clientX, e.changedTouches[0].clientY];
			} // touch
			else {
				if (e.which !== 1) {
					return;
				} // left mouse
				touchID = null;
				touchStartPosition = [e.clientX, e.clientY];
			} // mouse
			touchStartTime = Date.now();
			// console.debug(e.type+' touchID: ' + touchID);

			// Add new gesture if no previous dbltap started
			let gestures = dbltap_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				let new_gesture = {
					'element': elem,
					'touchID': touchID,
					'touchStartPosition': touchStartPosition,
					'touchStartTime': touchStartTime,
					'secondTouchID': null,
					'secondTouchStartPosition': null,
					'secondTouchStartTime': null
				};

				dbltap_gestures.push(new_gesture);
				// console.debug('New dbltap gesture started. touchID: ' + touchID);
				return;
			}

			// Check if this tap ends any existing gestures
			for (let i = 0, len = gestures.length; i < len; i++) {
				let gesture = gestures[i];

				// Check stray
				let has_strayed = false;
				let displacedX = touchStartPosition[0] - gesture['touchStartPosition'][0];
				let displacedY = touchStartPosition[1] - gesture['touchStartPosition'][1];
				if (displacedX > options['dbltapStrayXMax'] || 0 - displacedX > options['dbltapStrayXMax'] ||
					displacedY > options['dbltapStrayYMax'] || 0 - displacedY > options['dbltapStrayYMax']) {
					has_strayed = true;
				}

				// Check timeout
				let duration = touchStartTime - gesture['touchStartTime'];
				let has_timed_out = duration > options['dbltapIntervalMax'];

				if (has_strayed || has_timed_out) {
					// console.debug('stray or timeout. TouchID: ' + gesture['touchID'], displacedX, displacedY, duration + 'ms');
					removeGesture(dbltap_gestures, gesture);
				}
				else {
					gesture['secondTouchID'] = touchID;
					gesture['secondTouchStartPosition'] = touchStartPosition;
					gesture['secondTouchStartTime'] = touchStartTime;
					// console.debug('registered second touch', gesture);
				}
			}
		} // onDbltapGestureStart()

		function onDbltapGestureMove(e) {
			// Ignore this move event if no tap gestures exist
			let gestures = dbltap_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture, currPosition;
			if (e.type === 'touchmove') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = gestures.find(function(g) {
						return g['touchID'] === e.changedTouches[i].identifier || g['secondTouchID'] === e.changedTouches[i].identifier;
					});
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = gestures.find(function(g) {
					return g['touchID'] === null;
				});
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (!gesture) {
				return;
			}

			// Calculate movement and remove gesture if it strayed too far on second tap
			if (gesture['secondTouchStartTime']) {
				let displacedX = currPosition[0] - gesture['secondTouchStartPosition'][0];
				let displacedY = currPosition[1] - gesture['secondTouchStartPosition'][1];
				if (displacedX > options['tapStrayXMax'] || 0 - displacedX > options['tapStrayXMax'] ||
				    displacedY > options['tapStrayYMax'] || 0 - displacedY > options['tapStrayYMax']) {
					// console.debug('dbltap gesture cancelled. Move on 2nd tap, touchID: ' + gesture['touchID']);
					removeGesture(dbltap_gestures, gesture);
				}
			}
			else {
				// Calculate movement and remove gesture if it strayed too far
				let displacedX = currPosition[0] - gesture['touchStartPosition'][0];
				let displacedY = currPosition[1] - gesture['touchStartPosition'][1];
				if (displacedX > options['tapStrayXMax'] || 0 - displacedX > options['tapStrayXMax'] ||
				    displacedY > options['tapStrayYMax'] || 0 - displacedY > options['tapStrayYMax']) {
					// console.debug('dbltap gesture cancelled. Move on 1st tap, touchID: ' + gesture['touchID']);
					removeGesture(dbltap_gestures, gesture);
				}
			}
		} // onDbltapGestureMove()

		function onDbltapGestureEnd(e) {
			// Ignore this end event if no tap gestures exist
			let gestures = dbltap_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for second tap event, and position
			let gesture, currPosition;
			if (e.type === 'touchend') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = gestures.find(function(g) {
						return g['secondTouchID'] === e.changedTouches[i].identifier;
					});
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = gestures.find(function(g) {
					return g['secondTouchStartPosition'] !== null;
				});
				currPosition = [e.clientX, e.clientY];
			} // mouse
			if (gesture) {
				let duration = Date.now() - gesture['secondTouchStartTime'];
				if (duration > options['tapDurationMax']) {
					// console.debug('dbltap gesture cancelled. Second tap exceeded time ' + duration);
					removeGesture(dbltap_gestures, gesture);
					return;
				}

				// Emit "dbltap" if touch duration is within tap duration threshold
				elem.dispatchEvent(new CustomEvent('dbltap' + namespace, {
					'detail': {
						'startX': gesture['touchStartPosition'][0],
						'startY': gesture['touchStartPosition'][1],
						'endX': currPosition[0],
						'endY': currPosition[1]
					}
				}));
				removeGesture(dbltap_gestures, gesture);
				return;
			}

			// Find the gesture for first tap event, and the event's position
			if (e.type === 'touchend') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = gestures.find(function(g) {
						return g['touchID'] === e.changedTouches[i].identifier;
					});
					if (gesture) {
						currPosition = [e.changedTouches[i].clientX, e.changedTouches[i].clientY];
						break;
					}
				}
			} // touch
			else {
				gesture = gestures.find(function(g) {
					return g['secondTouchStartPosition'] === null;
				});
				currPosition = [e.clientX, e.clientY];
			} // mouse

			// Check if first tap exceed max tap duration
			if (gesture) {
				let duration = Date.now() - gesture['touchStartTime'];
				if (duration > options['tapDurationMax']) {
					// console.debug('dbltap gesture cancelled. First tap exceeded tapDurationMax ' + duration);
					removeGesture(dbltap_gestures, gesture);
				}
			}
		} // onDbltapGestureEnd()

		function onDbltapGestureCancel(e) {
			// Ignore this cancel event if no tap gestures exist
			let gestures = dbltap_gestures.filter(function(g) {
				return g['element'] === elem;
			});
			if (gestures.length === 0) {
				return;
			}

			// Find the gesture for this event, and the event's position
			let gesture;
			if (e.type === 'touchcancel') {
				for (let i = 0, len = e.changedTouches.length; i < len; i++) {
					gesture = findGestureByElementAndTouchID(dbltap_gestures, elem, e.changedTouches[i].identifier);
					if (gesture) {
						break;
					}
				}
			} // touch
			else {
				gesture = findGestureByElementAndTouchID(dbltap_gestures, elem, null);
			} // mouse
			if (!gesture) {
				return;
			}

			removeGesture(dbltap_gestures, gesture);
		} // onDbltapGestureCancel()

		function setUpTapListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onDbltapGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onDbltapGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onDbltapGestureEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onDbltapGestureCancel, EVENT_LISTENER_PARAM);
			}

			// Some browsers emulates mouse events on touchstart/touchend
			// If they support touch events, we should not enable mouse
			if (options['enableMouse'] && !('ontouchstart' in document.documentElement)) {
				elem.addEventListener('mousedown', onDbltapGestureStart, EVENT_LISTENER_PARAM);
				document.addEventListener('mousemove', onDbltapGestureMove, EVENT_LISTENER_PARAM);
				document.addEventListener('mouseup', onDbltapGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // setUpTapListeners()
		function tearDownTapListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onDbltapGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onDbltapGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onDbltapGestureEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onDbltapGestureCancel, EVENT_LISTENER_PARAM);
			}
			if (options['enableMouse']) {
				elem.removeEventListener('mousedown', onDbltapGestureStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('mousemove', onDbltapGestureMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('mouseup', onDbltapGestureEnd, EVENT_LISTENER_PARAM);
			}
		} // tearDownTapListeners()

		setUpTapListeners();

		return {
			'eventType': 'dbltap',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownTapListeners
		};
	} // createDblTapDetection()

	/**
	 * @private
	 * Starts emitting custom events: [pinchstart, pinchend] + [pinchin, pinchout]
	 * @param {HTMLElement} elem
	 *        {Object=} options - {}
	 *        {string} namespace - alphabetical only, without preceeding dot.
	 * @return {Object} detection
	 */
	function createPinchDetection(elem, options, namespace) {
		function onTouchStart(e) {
			let touch0ID, touch1ID, touch0Position, touch1Position, touchStartTime, distance;

			// Check if we have at least 2 touch points
			if (e.touches.length < 2) { // eslint-disable-line no-magic-numbers
				// console.debug('Less than 2 touch points - no pinch gesture started');
				return;
			}
			// console.debug('More than 2 touch points -  pinch gesture ready to start');

			// Check if we already have a pinch gesture
			let gesture = pinch_gestures.find(function(g) {
				return g['element'] === elem;
			});
			if (gesture) {
				// console.debug('Already have pinch gesture - not creating more');
				return;
			}


			touch0ID = e.touches[0].identifier;
			touch1ID = e.touches[1].identifier;
			touch0Position = [e.touches[0].clientX, e.touches[0].clientY];
			touch1Position = [e.touches[1].clientX, e.touches[1].clientY];
			touchStartTime = Date.now();
			distance = getDistance(touch0Position, touch1Position);

			// Create gesture
			let new_gesture = {
				'element': elem,
				'touch0ID': touch0ID,
				'touch1ID': touch1ID,
				'touch0StartX': touch0Position[0],
				'touch0StartY': touch0Position[1],
				'touch1StartX': touch1Position[0],
				'touch1StartY': touch1Position[1],
				'touchStartTime': touchStartTime,
				'startDistance': distance,
				'prevDistance': distance,
				'pinchStarted': false
			};
			pinch_gestures.push(new_gesture);
			// console.debug('Created pinch gesture', new_gesture);
		} // onTouchStart()

		function onTouchMove(e) {
			// Ignore this move event if no pinch gestures exist
			let gesture = pinch_gestures.find(function(g) {
				return g['element'] === elem;
			});
			if (!gesture) {
				return;
			}

			// Find the touches for this gesture
			let touch0, touch1;
			for (let i = 0, len = e.touches.length; i < len; i++) {
				if (e.touches[i].identifier === gesture['touch0ID']) {
					touch0 = e.touches[i];
				}
				if (e.touches[i].identifier === gesture['touch1ID']) {
					touch1 = e.touches[i];
				}
			} // for all changed touches
			if (!touch0 || !touch1) {
				// console.debug('Could not find both touches: ' + !!touch0 + ', ' + !!touch1);
				return;
			}

			// Check if pinch has occurred (distance changed)
			let curr_distance = getDistance([touch0.clientX, touch0.clientY], [touch1.clientX, touch1.clientY]);
			let delta_distance = Math.abs(curr_distance - gesture['prevDistance']);
			// console.debug('Moved ' + delta_distance);
			if (delta_distance < options['pinchDistanceThreshold']) {
				return;
			}

			// Details of this event
			let event_dict = {
				'detail': {
					'distance': curr_distance,
					'startDistance': gesture['startDistance'],
					'zoom': curr_distance / gesture['startDistance']
				}
			};

			// Emit "pinchstart" if not yet started
			if (!gesture['pinchStarted']) {
				gesture['pinchStarted'] = true;

				elem.dispatchEvent(new CustomEvent('pinchstart' + namespace, event_dict));
			}

			// Emit "pinchin" / "pinchout" accordingly
			if (curr_distance > gesture['prevDistance']) {
				elem.dispatchEvent(new CustomEvent('pinchout' + namespace, event_dict));
			} // increase
			else if (curr_distance < gesture['prevDistance']) {
				elem.dispatchEvent(new CustomEvent('pinchin' + namespace, event_dict));
			} // decrease
			gesture['prevDistance'] = curr_distance;
		} // onTouchMove()

		function onTouchEnd(e) {
			// Ignore this cancel event if no tap gestures exist
			let gesture = pinch_gestures.find(function(g) {
				return g['element'] === elem;
			});
			if (!gesture) {
				return;
			}

			// Check if we have at least 2 touch points
			if (e.touches.length < 2) { // eslint-disable-line no-magic-numbers
				// console.debug('Less than 2 touch points. ' + e.touches.length + ' Removing pinch gesture for this element.');
				// Emit pinchend (but only if pinchstart has previously been emitted)
				if (gesture['pinchStarted']) {
					elem.dispatchEvent(new CustomEvent('pinchend' + namespace, {
						'detail': {
							'endDistance': gesture['prevDistance'],
							'zoom': gesture['prevDistance'] / gesture['startDistance']
						}
					}));
				}

				removeGesture(pinch_gestures, gesture);
			}
		} // onTouchEnd()

		function onTouchCancel(e) {
			// Ignore this cancel event if no tap gestures exist
			let gesture = pinch_gestures.find(function(g) {
				return g['element'] === elem;
			});
			if (!gesture) {
				return;
			}

			// Check if we have at least 2 touch points
			if (e.touches.length < 2) { // eslint-disable-line no-magic-numbers
				// console.debug('Less than 2 touch points. ' + e.touches.length + ' Removing pinch gesture for this element.');
				// Emit pinchend (but only if pinchstart has previously been emitted)
				if (gesture['pinchStarted']) {
					elem.dispatchEvent(new CustomEvent('pinchend' + namespace, {
						'detail': {
							'endDistance': gesture['prevDistance'],
							'zoom': gesture['prevDistance'] / gesture['startDistance']
						}
					}));
				}

				removeGesture(pinch_gestures, gesture);
			}
		} // onTouchCancel()

		function setUpTapListeners() {
			if (options['enableTouch']) {
				elem.addEventListener('touchstart', onTouchStart, EVENT_LISTENER_PARAM);
				document.addEventListener('touchmove', onTouchMove, EVENT_LISTENER_PARAM);
				document.addEventListener('touchend', onTouchEnd, EVENT_LISTENER_PARAM);
				document.addEventListener('touchcancel', onTouchCancel, EVENT_LISTENER_PARAM);
			}
		} // setUpTapListeners()
		function tearDownTapListeners() {
			if (options['enableTouch']) {
				elem.removeEventListener('touchstart', onTouchStart, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchmove', onTouchMove, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchend', onTouchEnd, EVENT_LISTENER_PARAM);
				document.removeEventListener('touchcancel', onTouchCancel, EVENT_LISTENER_PARAM);
			}
		} // tearDownTapListeners()

		setUpTapListeners();

		return {
			'eventType': 'pinch',
			'element': elem,
			'namespace': namespace,
			'cleanup': tearDownTapListeners
		};
	} // createPinchDetection()


/****************
*    HELPERS    *
*****************/
	/**
	 * @private
	 * Gets distance between 2 points
	 * @param {Array} p1
	 *        {Array} p2
	 * @return {number}
	 */
	function getDistance(p1, p2) {
		let x = p1[0] - p2[0];
		let y = p1[1] - p2[1];
		return Math.sqrt((x * x) + (y * y));
	} // getDistance()

	/**
	 * @private
	 * Checks if a detector for an element is already set up
	 * @param {HTMLElement} elem
	 *        {string} event_type
	 *        {string} namespace
	 * @return {boolean}
	 */
	function isDetecting(elem, event_type, namespace) {
		for (let i = 0, len = detectors.length; i < len; i++) {
			let detector = detectors[i];
			if (detector['element'] === elem && detector['eventType'] === event_type && detector['namespace'] === namespace) {
				return true;
			}
		}
		return false;
	} // isDetecting()

	/**
	 * @private
	 * Removes a detector
	 */
	function removeDetector(to_remove) {
		for (let i = 0, len = detectors.length; i < len; i++) {
			let detector = detectors[i];
			if (detector['element'] === to_remove['element'] &&
				detector['eventType'] === to_remove['eventType'] &&
				detector['namespace'] === to_remove['namespace']) {
				detectors.splice(i, 1);
				return;
			}
		}
	} // removeDetector()

	/**
	 * @private
	 * Finds a gesture by its element and the touchID of its orignating touch
	 * @param {Array} gestures - the list of gestures to search from, e.g. pan_gestures
	 *        {HTMLElement} elem
	 *        {number} touchID
	 * @return {Object | null} Gesture object
	 */
	function findGestureByElementAndTouchID(gestures, elem, touchID) {
		for (let i = 0, len = gestures.length; i < len; i++) {
			let gesture = gestures[i];
			if (gesture['element'] === elem && gesture['touchID'] === touchID) {
				return gesture;
			}
		}
		return null;
	} // findGestureByElementAndTouchID()

	/**
	 * @private
	 * Removes a Gesture from its registry
	 * @param {Array} gestures - the list of gestures to search from, e.g. pan_gestures
	 *        {Object} gestureToRemove
	 */
	function removeGesture(gestures, gestureToRemove) {
		for (let i = 0, len = gestures.length; i < len; i++) {
			let gesture = gestures[i];
			if (gesture['element'] === gestureToRemove['element'] && gesture['touchID'] === gestureToRemove['touchID']) {
				gestures.splice(i, 1);
				return;
			}
		}
	} // removeGesture()

/*********
*  Init  *
**********/
	function handleDocumentMutation(mutations) {
		// Get list of removed nodes
		let removed_nodes = mutations.flatMap(function(m) {
			return Array.from(m.removedNodes);
		});
		let removed_nodes_len = removed_nodes.length;

		// Filter detectors in place
		let new_index = 0;
		for (let i = 0, len = detectors.length; i < len; i++) {
			let detector = detectors[i];

			// Check if this element is removed
			let dom_elem = detector['element'];
			let is_removed = false;
			for (let j = 0; j < removed_nodes_len; j++) {
				if (removed_nodes[j].contains(dom_elem)) {
					is_removed = true;
					break;
				}
			}

			// Disconnect intersection observer if element is removed
			if (is_removed) {
				detector.cleanup();
			}
			else {
				detectors[new_index++] = detector;
			}
		}
		detectors.length = new_index;
	} // handleDocumentMutation()

	// Set up Mutation Observer to listen for DOM removals
	const mutation_observer = new MutationObserver(handleDocumentMutation);
	mutation_observer.observe(document.body, {'childList': true, 'subtree': true});

	global['tove'] = {
		'detect': detect,
		'stopDetect': stopDetect,
		'getDetectors': getDetectors,
		'getDetectorCount': getDetectorCount,
		'getPanGestureCount': getPanGestureCount,
		'getPressGestureCount': getPressGestureCount,
		'getSwipeGestureCount': getSwipeGestureCount,
		'getTapGestureCount': getTapGestureCount,
		'getDbltapGestureCount': getDbltapGestureCount,
		'getPinchGestureCount': getPinchGestureCount,
		'getRotateGestureCount': getRotateGestureCount
	};
}(this));
