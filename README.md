# Tove - Custom Touch Events #

A JavaScript library for detecting custom touch events, and emits them as DOM CustomEvents.

### Event Types: ###

**Pan**: pointer/touch begins on target element, and moves to anywhere in the document.

* `[panstart, panend]` triggered at beginning and end of the move.
* `[panleft, panright, panup, pandown]` triggered at any point during the move, if pointer/touch is displaced more than the threshold.

**Press**: pointer/touch begins on target element, held for a period of time.

* `[pressdown]` triggered if held down for sufficient time, a pressdown event is triggered.

* `[pressup]` triggered on release. At any point, if significant movement is detected, the press gesture is cancelled, i.e. pressup will not be triggered.

**Tap**: pointer/touch begins on a target element, and released quickly.

* `[tap]` triggered if released before threshold duration, and without movement.
  - if held down for a signficant amount of time, the tap gesture is cancelled.
  - at any point, if significant movement is detected, the tap gesture is cancelled.

 
**Swipe**: Pointer/touch begins on target element, moves, and ends anywhere in the document.

* `[swipeleft, swiperight, swipeup, swipedown]` triggered if total displacement within the time elapsed exceeds a certain velocity in the x-axis or y-axis.

### Features ###
* Handles both Pointer Events and Touch Events
* Fast! Event-based detection.
* Uses native DOM CustomEvents

### Browser Support ###
* Chrome
* Edge
* Firefox
* Opera
* Safari

### Set Up ###

1. Download the latest release (as of [29 Feb 2020, v2.2.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="tove-2.1.1.min.js"></script>`
    
3. Also consider including polyfill to normalize your target environments: [DOM CustomEvent](https://github.com/krambuhl/custom-event-polyfill)

### API ###
**detect**

```
/**
 * Starts emitting custom events based on the type entered.
 * Note: native touch behaviour will be disabled.
 * @param: {HTMLElemet | string} elem - if string, querySelectorAll will be applied to all elements matching the string
 *         {string} eventType - space-separated types of events [pan, press, tap, swipe]
 *         {object} userOptions (optional) - will be merged into default options
 *         {string} namespace (optional)
 * @return: {undefined}
 */

// Sample usage (simple):
var elem = document.getElementById('my-elem');
tove.detect(elem, 'press');
elem.addEventListener('pressup', function(e) { // define listeners for events like this
    console.log('Pressed up');
	console.log(e);
});
elem.addEventListener('pressdown', function(e) {
    console.log('Pressed down');
	console.log(e);
});

// Sample usage (even simpler): 
tove.detect('#my-elem', 'swipe');  // detect swipes on #my-elem
tove.detect('.castle', 'press');   // detect presses in all castles

// Sample usage (custom options)
var elem = document.getElementById('my-elem');
var eventTypes = 'all';                  // shorthand for "pan press tap swipe"
var userOptions = {                      // these are the actual defaults.
	// pan
	panXThreshold: 5, // min px required for a pan in x-axis
	panYThreshold: 5, // min px required for a pan in y-axis
	
	// press
	pressStrayXMax: 20, // max px of stray allowed after touch starts, before touch ends
	pressStrayYMax: 20,
	pressDurationThreshold: 500, //min duration (ms) required for a pressdown
	
	// tap
	tapStrayXMax: 5, // max px of stray allowed after touch starts, before touch ends
	tapStrayYMax: 5,
	tapDurationMax: 200, // max duration (ms) for a tap
	
	// swipe
	swipeXThreshold: 20, // min px required for a swipe in x-axis
	swipeYThreshold: 20, // min px required for a swipe in y-axis
	swipeVelocityXThreshold: 0.7, //min velocity in x-axis required for swipeleft, swiperight (px per ms)
	swipeVelocityYThreshold: 0.7, //min velocity in y-axis required for swipeup, swipedown (px per ms)
	
	// global
	enableMouse: true,
	enableTouch: true,
	enablePen: true
};
tove.detect(elem, eventTypes, userOptions, 'greyjoy'); // start emitting custom events

elem.addEventListener('panup.greyjoy', function(e) { // define listeners for events like this
    console.log('Panned up');
	console.log(e);
});
elem.addEventListener('pandown.greyjoy', function(e) {
    console.log('Panned down');
	console.log(e);
});
```

**stopdetect**

```
/**
 * Stops detecting custom touch events
 * @param: {HTMLElemet | string} elem - if string, querySelectorAll will be applied to all elements matching the string
 *         {string} eventType - space-separated types of events [pan, press, swipe]
 *         {string} namespace (optional)
 */
 
// Sample usage:
tove.stopDetect(elem, 'tap', 'bolton');   // stop emitting "tap.bolton" events
```

**getDetectorCount**

```
/**
 * Returns number of detectors set up. Note: each event type on an element counts as one detector.
 */

// Sample usage:
var count = tove.getDetectionCount();
console.log('There are ' + count + ' detectors.');
```

**getPanGestureCount / getPressGestureCount / getSwipeGestureCount / getTapGestureCount**
```
/**
 * Returns the number of active gestures currently being tracked.
 */

// Sample usage:
var count = tove.getPanGestureCount();
console.log('There are ' + count + ' pans being tracked right now.');
```
 
### FAQ ###

1. Why don't you simply use HammerJS or other existing touch event libraries?
> We have not found a library with all the nice features we need. A lot of libraries are buggy or use inefficient algorithms. Almost all other libraries also create their own "event objects", which is a shame. We prefer to use the standard Web API CustomEvent for this purpose.
>
> We designed this library to automatically detect sensible defaults so that if you don't specify the settings, things work as you'd expect them to. Refer to the API above.

1. Is it faster than HammerJS?
> We have no idea, but we definitely hope so.

1. Is there a method to clear all custom events regardless of namespace?
> No.

1. Is there a demo?
> Yes, do a git checkout of the project and look inside `demo` folder.
   
### Contact ###

* Email us at <yokestudio@hotmail.com>.